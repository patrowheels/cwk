my_rec = codesters.Rectangle(
    200, # x pos
    100, # y pos
    20, # width
    50, # height
    "blue", # color
)

counter = 0
while True:
    stage.wait(.1)
    counter = counter + 1
    if counter % 10 == 0:
        old_x = my_rec.get_x()
        new_x = old_x - 10
        my_rec.set_x(new_x)
