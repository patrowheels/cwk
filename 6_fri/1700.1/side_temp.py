# ===== SETUP GROUNDED TEXT =====

grounded_text = codesters.Text("Grounded: True")

def update_grounded_text():
    grounded_text.set_text(f"Grounded: {hero.grounded}")

def space_bar():
    if hero.grounded == True:
        hero.grounded = False
        hero.set_gravity_on()
        hero.jump(5)

stage.event_key("space", space_bar)

def collision(in_hero, in_object):
	object_color = in_object.get_color()
	if object_color == "darkred":
		hero.set_gravity_off()
		hero.grounded = True
		hero.set_y_speed(0)

hero.event_collision(collision)
