# ===== SETUP ENEMY =====

my_rec = codesters.Rectangle(
    200, # x pos
    -243 + 50, # y pos
    20, # width
    50, # height
    "blue", # color
)

# ===== SETUP HERO =====

hero = codesters.Sprite("superhero1")
hero.set_size(.5)
hero.set_x(-150)
hero.set_y(
    # floor position + 
    # 1/2 floor height +
    # 1/2 hero height
    -243 + 25 + (hero.get_height()/2)    
)
hero.grounded = True # new code

# ===== SETUP GROUNDED TEXT =====

grounded_text = codesters.Text("Grounded: True")

def update_grounded_text():
    ground_text.set_text(f"Grounded: {hero.grounded}")

def space_bar():
    if hero.grounded == True:
        hero.grounded = False
        hero.set_gravity_on()
        hero.jump(5)

stage.event_key("space", space_bar)

counter = 0
while True:
    stage.wait(.1)
    counter = counter + 1
    if counter % 10 == 0:
        old_x = my_rec.get_x()
        new_x = old_x - 10
        my_rec.set_x(new_x)

