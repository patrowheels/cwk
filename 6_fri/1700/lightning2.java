public class Player extends BasePlayer {
	public void OnRunCommand(String message) {
		if (message.equals("/makelightning")) {
			world.strikeLightning(getTargetBlock().getLocation());
		}
		if (message.equals("/invisible")) {
			applyPotionEffect(PotionEffectType.INVISIBILITY, 600, 1);
		}
		if (message.equals("/heal")) {
			setFoodLevel(20);
			setHealth(20);
		}
		if (message.equals("/speed")) {
			
			setWalkSpeed(0.5f);
			
		}
		if (message.equals("/sword")) {
			
			addItemToInventory(new ItemStack(Material.DIAMOND_SWORD));
			
		}


	}
}
