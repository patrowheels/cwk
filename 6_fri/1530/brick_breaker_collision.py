ball.collision_on()

def change_y_velocity(in_ball):
    # makes the ball bounce up/down when it hits something
    in_ball.set_y_speed(
        in_ball.get_y_speed() * -1
    )



def collision(sprite, hit_sprite):
    if hit_sprite.get_color() == "yellow":
        change_y_velocity(ball) # bounces
    if hit_sprite.get_color() == "green":
        change_y_velocity(ball) # bounces
        stage.remove_sprite(hit_sprite) # deletes a brick
    
ball.event_collision(collision)
