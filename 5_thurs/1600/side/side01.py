sprite = codesters.Sprite(
    "alien1_masked", # image name
    -150, # x
    -100, # y
)

floor = codesters.Rectangle(
	0, # x
	-200, # y
	500, # width
	100, # height
	"darkred", # color
)

stage.set_gravity(10)

