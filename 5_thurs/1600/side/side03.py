alien = codesters.Sprite(
    "alien1_masked", # image name
    -150, # x
    -50, # y
)

floor = codesters.Rectangle(
    0, # x
    -250, # y
    500, # width
    100, # height
    "darkred", # color
)

floor.set_gravity_off()

stage.set_gravity(10)

# ===== SETUP GROUNDED =====

alien.grounded = False

grounded_text = codesters.Text(
    f"grounded: {alien.grounded}", # text
    -160, # x
    200, # y
)

def collision_chicken(sprite_icecream, other_pizza):
    other_color_icepops = other_pizza.get_color()
    if other_color_icepops == "darkred": # COLLIDE W/GROUND
        sprite_icecream.set_gravity_off()
        sprite_icecream.set_y_speed(0)
        sprite_icecream.grounded = True # NEW
        grounded_text.set_text( # NEW
            f"grounded: {sprite_icecream.grounded}" # NEW
        ) # NEW
    if other_color_icepops == "blue": # COLLIDE W/BLUE ENEMY
        sprite_icecream.say("ouch!", 2)

alien.event_collision(collision_chicken)

def space_bar_dubai(sprite_tokyo):
    sprite_tokyo.jump(10)
    sprite_tokyo.set_gravity_on()
    sprite_tokyo.grounded = False # NEW
    grounded_text.set_text(f"grounded: {sprite_tokyo.grounded}") # NEW
    
alien.event_key("space", space_bar_dubai)

def left_key(sprite):
    sprite.move_left(20)

alien.event_key("left", left_key)

def right_key(sprite):
    if sprite.get_x() < 0:
        sprite.move_right(20)

alien.event_key("right", right_key)



"""
Dubai
Tokyo
Sana Barbra
London
"""

"""
GOALS:

* the sprite can move left/right a bit
* generate obstacles

ADVANCED GOALS:
* drop "down key" to drop
* add double jump
* add health
* create an end screen
"""

