floor = codesters.Rectangle(
    0, # x
    -225, # y
    500, # width
    50, # height
    "darkred", # color
)

floor.id = "main floor"
floor.set_gravity_off()

stage.set_gravity(10)

def make_bouncy_enemy():
    new_enemy = codesters.Rectangle(
        300, # x
        200, # y
        20, # width
        20, # height
        "blue", # color
    )
    new_enemy.id = "bouncy enemy"
    new_enemy.set_x_speed(-5)
    
make_bouncy_enemy()
