# <---- numbersign/hashtag, Shift+3
# filename: rps.py
# chapter 1
import random

print("This is rock paper scissors!")
print("Choose your weapon")
user_weapon = input("choose: ")
print("you chose", user_weapon)

comp_weapon = random.choice(["rock", "paper", "scissors"])
print("I chose", comp_weapon)
# chapter 2
if comp_weapon == "rock":
  if user_weapon == "paper":
    print("You win!")
  if user_weapon == "scissors":
    print("You lose!")
  if user_weapon == "rock":
    print("Tie!")
# copy the two lines above, adjust them
# for "draw" and "lose"
