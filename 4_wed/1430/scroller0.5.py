hero = codesters.Sprite("alien1_masked")
stage.disable_all_walls()

def collision(sprite, hit_sprite):
    shape_color = hit_sprite.get_color() 
    if shape_color == "blue":
        sprite.say("Ouch!", .1)

    
hero.event_collision(collision)




counter = 0
while True:
    stage.wait(.1)
    counter = counter + 1
    if counter % 20 == 0:
        new_rec = codesters.Rectangle(
            200, 0, 50, 50, "blue"
        )
        new_rec.set_x_speed(-5)
