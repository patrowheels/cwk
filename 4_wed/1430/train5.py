# ========== SETUP HERO ==========

hero = codesters.Sprite("alien1", -200, 0)
hero.set_size(.3)
hero.lives = 3

# ========== SETUP TEXT ==========
lives_text = codesters.Text(
    f"lives: {hero.lives}", -200, 200    
)

# ========== SETUP STAGE ==========

stage.disable_all_walls()

# ========== SETUP MOVEMENT ==========

def up_key(sprite):
    sprite.move_up(20)
    
def down_key(sprite):
    sprite.move_down(20)

def right_key(sprite):
    sprite.move_right(25)
    sprite.say(sprite.get_x())
    
def left_key(sprite):
    sprite.move_left(25)
    sprite.say(sprite.get_x())

hero.event_key("up", up_key)
hero.event_key("down", down_key)
hero.event_key("right", right_key)
hero.event_key("left", left_key)

# ========== SETUP COLLISION ==========

def collision(sprite, hit_sprite):
    my_var = hit_sprite.get_color() 
    if my_var == "blue" and sprite.get_x() == hit_sprite.get_x():
        sprite.say("Ouch!")
        sprite.lives = sprite.lives - 1
        lives_text.set_text(f"lives: {sprite.lives}")
        
    if my_var == "green":
        sprite.say("Goal!")
        
hero.event_collision(collision)

# ========== SETUP TRAIN ==========

def make_train(dir=1):
    random_x_pos = random.randint(-5, 5) * 25
    y_pos = 200 * dir
    width = 15
    height = 40
    for i in range(3):
        train = codesters.Rectangle(
            random_x_pos, # x
            y_pos + (i * (height + 5)) * dir, # y
            width, # width
            height, # height
            "blue", # color
        )
            
        train.set_y_speed(-10 * dir)

# ========== GAME LOOP ==========

while True:
    stage.wait(.6)
    make_train(
        dir = random.choice([-1, 1])
    )

"""
GOALS:
* Coins
* Lives
"""
