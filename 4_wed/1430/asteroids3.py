# SET UP SPRITE

sprite_roblox = codesters.Sprite("ufo", 0, -175)
sprite_roblox.set_size(0.3)
sprite_roblox.health = 10

# SET UP STAGE

stage.disable_all_walls()
stage.set_background("space")

# SET UP MOVEMENT

def left_sushi(sprite):
    sprite.move_left(20)

def right_fries(sprite):
    sprite.move_right(20)
    
sprite_roblox.event_key("left", left_sushi)
sprite_roblox.event_key("right", right_fries)

def collision(sprite, hit_sprite):
    sprite.say("collision function running")
    hit_sprite.say("gotcha!")
    """
    my_var = hit_sprite.get_image_name() 
    if my_var == "codesters":
        sprite.say("I hit something!")
    """
    
sprite_roblox.event_collision(collision)




# loop forever
while True:
    asteroid = codesters.Sprite(
        "asteroid", 
        random.randint(-150, 150), 
        250,
    )
    
    asteroid.set_size(
        random.randint(2, 4) * .1
    )
    
    asteroid.set_y_speed(-2)
    asteroid.set_x_speed(random.randint(-5, 5))
    sprite_roblox.say(sprite_roblox.health)
    stage.wait(1)


"""
# DIFFERENT WAYS TO SET A RANDOM SIZE
asteroid.set_size(
    random.choice([.2, .3, .4])
)

asteroid.set_size(
    random.random() * .2 + .2
)
"""



