# SET UP SPRITE

sprite_roblox = codesters.Sprite("ufo", 0, -175)
sprite_roblox.set_size(0.3)
sprite_roblox.health = 10

# SET UP STAGE

stage.disable_all_walls()
stage.set_background("space")

# SET UP MOVEMENT

def left_sushi(sprite):
    sprite.move_left(20)

def right_fries(sprite):
    sprite.move_right(20)
    
sprite_roblox.event_key("left", left_sushi)
sprite_roblox.event_key("right", right_fries)

# SET UP COLLISION

def collision(me, not_me):
    
    not_me_name = not_me.get_image_name()
    
    if not_me_name == "anemone":
        sprite_roblox.health = sprite_roblox.health + 10
        stage.remove_sprite(not_me)
        
    elif not_me_name == "asteroid":
        sprite_roblox.health = sprite_roblox.health - 1
        stage.remove_sprite(not_me)

    
sprite_roblox.event_collision(collision)

# SINGLE-FIRE ANEMONE SPRITE

tempsprite = codesters.Sprite(
    "anemone", 
    0, 
    250,
)
tempsprite.set_size(
    random.randint(2, 4) * .1
)
tempsprite.set_y_speed(-4)

# loop forever
while True:
    
    # CREATE AN ASTEROID
    asteroid = codesters.Sprite(
        "asteroid", 
        random.randint(-150, 150), 
        250,
    )
    asteroid.set_size(
        random.randint(2, 4) * .1
    )
    
    asteroid.set_y_speed(-2)
    asteroid.set_x_speed(random.randint(-2, 2))
    
    # ANOUNCE HEALTH
    sprite_roblox.say(sprite_roblox.health)
    
    # WAIT BEFORE LOOPING AGAIN
    stage.wait(.3)

"""
GOALS

* Make a powerup that makes our ship go faster
  * The powerup comes occasionally
* Put a score in the top left corner
"""



