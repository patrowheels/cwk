# SET UP SPRITE

sprite = codesters.Sprite("ufo", 0, -175)
sprite.set_size(0.3)

# SET UP MOVEMENT

def left_sushi(sprite):
    sprite.move_left(20)

def right_fries(sprite):
    sprite.move_right(20)
    
sprite.event_key("left", left_sushi)
sprite.event_key("right", right_fries)
