stage.disable_all_walls()
stage.set_gravity(10)

hero = codesters.Sprite("alien2")
hero.set_gravity_on()

def space_bar():
    hero.jump(5)

stage.event_key("space", space_bar)

ground = codesters.Rectangle(
    0, # x position
    -225, # y position
    500, # width
    50, # height
    "limegreen",
)
ground.set_gravity_off()

def collision(sprite, hit_sprite):
    my_var = hit_sprite.get_color() 
    if my_var == "red":
        sprite.say("Ouch!")
        
    if my_var == "limegreen": # changed "green" -> "limegreen"
        sprite.say("Goal!")
        
hero.event_collision(collision) # changed "sprite" -> "hero"
