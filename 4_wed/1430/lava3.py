# SETUP STAGE

stage.set_gravity(5)
stage.disable_all_walls()

# SETUP ALIEN

alien = codesters.Sprite("alien2", 0, 50)
alien.set_size(0.5)
alien.grounded = False # changed

# SETUP PLATFORM

platform = codesters.Rectangle(0, -30, 75, 10, "blue")
platform.set_gravity_off()
platform2 = codesters.Rectangle(0, 150, 75, 10, "blue")
platform2.set_gravity_off()


# SETUP GRAVITY TEXTBOX
grounded_text = codesters.Text(f"Grounded: {alien.grounded}", 
    -200, 200) # changed
def refresh_grounded_text(): # changed
    grounded_text.set_text(f"Grounded: {alien.grounded}") # changed

def collision(me, not_me):
    not_me_color = not_me.get_color()
    if not_me_color == "blue" and me.get_y_speed() < 0:
        
        # make it like you're standing on a floor
        me.set_gravity_off()
        me.set_y_speed(0)
        
        # handle the alien's state, in air or on ground
        alien.grounded = True # changed
        refresh_grounded_text() # changed
        
        
        legs = me.get_height() / 2 - 5
        me.set_y(not_me.get_y() + legs)

alien.event_collision(collision)

def space_bar_grinch():
    if alien.grounded == True: # changed
        alien.jump(10) 
        alien.grounded = False # changed
        alien.set_gravity_on()
        refresh_grounded_text() # changed

stage.event_key("space", space_bar_grinch)

"""
GOALS:
  * Debug getting stuck in the platform
  * Make platforms move
  * Make the alien's feet touch the top of the platforn
"""
