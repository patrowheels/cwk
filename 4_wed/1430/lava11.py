# SETUP STAGE
stage.set_gravity(5)
stage.disable_all_walls()

# SETUP ALIEN
alien = codesters.Sprite("alien2", 0, 120)
alien.set_size(0.5)
alien.grounded = False
alien.platform = None
alien.score = 0


# SETUP PLATFORMS

platform_list = []

platform = codesters.Rectangle(0, - 30, 75, 10, "blue")
platform.set_gravity_off()
platform.name = "Chicago"
platform.left_side = platform.get_x() - (platform.get_width()/2)
platform.right_side = platform.get_x() + (platform.get_width()/2)
platform.set_x_speed(-1)
platform_list.append(platform)


platform2 = codesters.Rectangle(175, 150, 75, 10, "blue")
platform2.set_gravity_off()   
platform2.set_x_speed(3)
platform2.set_y_speed(-1)
platform2.name = "NYC"
platform_list.append(platform2)

lava = codesters.Rectangle(0, -250, 500, 100, "red")
lava.set_gravity_off()
lava.move_to_front()

def name_gen():
    adjectives1 = ["quick", "slow", "brown", "green", "orange"]
    adjectives2 = ["happy", "sad", "blue", "yellow", "purple"]
    animals = ["fox", "cat", "dog", "mouse", "bear"]
    sports = ["football", "basketball", "soccer", "baseball"]
    return random.choice(adjectives1) + \
        random.choice(adjectives2) + \
        random.choice(animals) + \
        random.choice(sports)

def create_platform():
    new_platform = codesters.Rectangle(175, 150, 125, 10, "blue")
    new_platform.set_gravity_off()   
    new_platform.set_x_speed(
        random.randint(2, 7)
        )
    new_platform.set_y_speed(-1)
    new_platform.name = name_gen()
    print(new_platform.name)
    platform_list.append(new_platform)
    lava.move_to_front()
                   
# SETUP GRAVITY TEXTBOX
grounded_text = codesters.Text(f"Grounded: {alien.grounded}", 
    -180, 200) # changed
def refresh_grounded_text():
    grounded_text.set_text(f"Grounded: {alien.grounded}")

# SETUP PLATFORM TEXTBOX
platform_text = codesters.Text(f"Platform: {alien.platform}", 
    -180, 175) # changed
def refresh_platform_text(): # changed
    platform_text.set_text(f"Platform: {alien.platform}")

# SETUP SCORE TEXT
score_text = codesters.Text(f"Score: {alien.score}", -180, 150)
def refresh_score_text():
    score_text.set_text(f"Score: {alien.score}")

# SETUP COLLISION
def collision(me, not_me):
    not_me_color = not_me.get_color()
    if all([
        # colliding with something blue
        not_me_color == "blue",
        # not moving up
        me.get_y_speed() <= 0,
        # the alien is slightly above the platform
        me.get_y() > not_me.get_y() + 10,
        # the alien's not too far left
        me.get_x() > not_me.get_x() - (not_me.get_width()/2),
        # the alien's not too far right
        me.get_x() < not_me.get_x() + (not_me.get_width()/2),
        not alien.grounded,
    ]):
        
        # update score
        alien.score += 1
        refresh_score_text()
        
        # make it like you're standing on a floor
        me.set_gravity_off()

        # handle the alien's state, in air or on ground
        alien.grounded = True
        refresh_grounded_text()
        
        # snap to correct height
        legs = me.get_height() / 2 - 5
        me.set_y(not_me.get_y() + legs)
        
        # take platforms speed
        me.set_x_speed(not_me.get_x_speed())
        me.set_y_speed(not_me.get_y_speed())
        
        # remember which platform
        alien.platform = not_me.name
        refresh_platform_text()

alien.event_collision(collision)
# sprite = codesters.Sprite("candy")

def space_bar_grinch():
    if alien.grounded == True:
        alien.jump(10)
        alien.set_x_speed(0)
        alien.grounded = False
        alien.set_gravity_on()
        refresh_grounded_text()
        alien.platform = None
        refresh_platform_text()

stage.event_key("space", space_bar_grinch)


def check_platform_bounce():
    for one_platform in platform_list:
        if one_platform.get_x() > 150 and one_platform.get_x_speed() > 0:
            one_platform.set_x_speed(one_platform.get_x_speed() * -1)
            if alien.platform == one_platform.name:
                alien.set_x_speed(one_platform.get_x_speed())
        elif one_platform.get_x() < -150 and one_platform.get_x_speed() < 0:
            one_platform.set_x_speed(one_platform.get_x_speed() * -1)
            if alien.platform == one_platform.name:
                alien.set_x_speed(one_platform.get_x_speed())


def check_lose():
    if alien.get_y() < -230:
        lose_text = codesters.Text("YOU LOSE")
        return True
    else:
        return False

game_counter = 0
game_running = True
while game_running == True:
    game_counter += 1
    if game_counter % 20 == 0:
        create_platform()
    stage.wait(.1)
    check_platform_bounce()
    game_running = not check_lose()

