# divide_and_remainder.py
numerator = int(input(": "))
denomenator = int(input(": "))
print(
  f"{numerator} divided by {denomenator} is",
  f"{numerator//denomenator}, with a remainder of",
  numerator % denomenator
)
