class Sprite:
  def __init__(self):
    self.x_pos = 200
    self.y_pos = 0

  def move_left(self):
    self.x_pos = self.x_pos - 20

  def print_position(self):
    print(f"x: {self.x_pos}, y: {self.y_pos}")

mySprite = Sprite()
mySprite.print_position()
mySprite.move_left()
mySprite.print_position()
