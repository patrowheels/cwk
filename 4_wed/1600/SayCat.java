import java.util.Scanner;
class SayCat {
	public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in);
		boolean keepGoing = true;
		String userAnswer;
		while (keepGoing == true) {
			System.out.println("Say cat: ");
			userAnswer = myScanner.next();
			if (userAnswer.equals("cat")) {
				keepGoing = false;
			}
		}
	}
}
