sprites = []
for i in range(2):
    """
    sprites.append(codesters.Sprite("evilknight", 500, 500))
    sprites.append(codesters.Sprite("ufo", 500, 500))
    sprites.append(codesters.Sprite("alien1_masked", 500, 500))
    """
    for sprite_name in ["evilknight", "ufo", "alien1_masked",
    "anemone", "butterfly", "shark7", "snowman",
    "present1", "rock", "scissors"]:
        sprites.append(codesters.Sprite(sprite_name, 500, 500))
    
    
for one_sprite in sprites:
    one_sprite.set_size(.2)

"""
# sprite = codesters.Rectangle(x, y, width, height, "color")
sprite = codesters.Rectangle(-150, 0, 50, 75, "blue")
sprite = codesters.Rectangle(-75, 0, 50, 75, "blue")
sprite = codesters.Rectangle(0, 0, 50, 75, "blue")
sprite = codesters.Rectangle(75, 0, 50, 75, "blue")
sprite = codesters.Rectangle(150, 0, 50, 75, "blue")
"""
"""
# for x_pos in range(-150, 151, 75):
for x_pos in [-150, -75, 0, 75, 150]:
    sprite = codesters.Rectangle(x_pos, 0, 50, 75, "blue")
    
for x_pos in [-150, -75, 0, 75, 150]:
    sprite = codesters.Rectangle(x_pos, 100, 50, 75, "blue")
    
for x_pos in [-150, -75, 0, 75, 150]:
    sprite = codesters.Rectangle(x_pos, 200, 50, 75, "blue")
"""

random.shuffle(sprites)

for y_pos in [-100, 0, 100, 200]: # added -100
    for x_pos in [-150, -75, 0, 75, 150]:
        sprite = codesters.Rectangle(x_pos, y_pos, 50, 75, "blue")


