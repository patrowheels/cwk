# MAKE SPRITES

sprite = codesters.Sprite("astronaut7", 0, 150)
sprite.set_size(0.15)
sprite.standing_on = None
sprite.score = 0

def set_sides(in_platform):
    in_platform.left_side = in_platform.get_x() - (in_platform.get_width() / 2 )
    in_platform.right_side = in_platform.get_x() + (in_platform.get_width() / 2 )
    in_platform.top_side = in_platform.get_y() + (in_platform.get_height() / 2 )
    
# CREATE PLATFORMS

platforms = []
for x, y, width, height, food_name in [
    (0, 0, 100, 25, "breakfast crepes"),
    (0, -100, 100, 25, "dinner crepes"),
    (0, -225, 400, 50, "waffles"),
    # (-200, 0, 50, 300, "pancakes"),
]:
    platform1 = codesters.Rectangle(x, y, width, height, "darkred")
    platforms.append(platform1)
    platform1.set_gravity_off()
    platform1.name = food_name
    set_sides(platform1)

# CREATE COINS

for x_coordinate in [-115, 0, 115]:
    coin = codesters.Sprite("coin", x_coordinate, -180)
    coin.set_gravity_off()
    coin.set_size(.5)

# CREATE FLOWER

flower = codesters.Sprite("flower1", 200, -180)
flower.set_gravity_off()

# HANDLE GRAVITY

stage.set_gravity(10)

# DEBUGGING TOUCHING GROUND

sprite.grounded = False
grounded_text = codesters.Text(f"grounded: {sprite.grounded}", -175, 200)
platform_text = codesters.Text(f"platform: {None}", -175, 175)
name_text = codesters.Text(f"object name: {None}", -175, 150)
score_text = codesters.Text(f"score: {sprite.score}", 175, 175)

def update_grounded_text():
    grounded_text.set_text(f"grounded: {sprite.grounded}")
    
def update_platform_text():
    platform_text.set_text(f"platform: {sprite.standing_on.name}")

def update_name_text(in_string):
    name_text.set_text(f"object name: {in_string}")
    
def update_score_text():
    score_text.set_text(f"score: {sprite.score}")

def collision(in_sprite, hit_sprite):
    shape_color = hit_sprite.get_color()
    shape_name = hit_sprite.get_name()
    update_name_text(shape_name)
    if shape_color == "darkred" and not sprite.grounded:
        feet = sprite.get_y() - (sprite.get_height() / 2)
        if (in_sprite.get_y_speed() < 0 # falling
        and feet > hit_sprite.get_y() # feet above the middle of the platform
        ):
            in_sprite.set_y_speed(0) # no longer falling
            in_sprite.set_gravity_off() # won't be pulled
            in_sprite.set_y( # make the sprite stand just above the platform
                hit_sprite.top_side +
                (in_sprite.get_height() / 2)
            )
            # HANDLE GROUNDED
            sprite.grounded = True
            update_grounded_text()
            # HANDLE STANDING_ON
            sprite.standing_on = hit_sprite
            update_platform_text()
        elif in_sprite.get_y_speed() > 0: 
            in_sprite.set_y_speed(0)
    elif shape_name == "coin":
        stage.remove_sprite(hit_sprite)
        sprite.score += 1
        update_score_text()
    elif shape_name == "flower1":
        sprite.grounded = False
        sprite.set_gravity_on()
        sprite.set_y_speed(20)
        
        



sprite.event_collision(collision)

def jump_clash_royale():
    if sprite.grounded == True: # if the sprite is on the ground
        sprite.grounded = False # the sprite is no longer on the ground
        update_grounded_text()
        sprite.jump(15)
        sprite.set_gravity_on()
    
stage.event_key("space", jump_clash_royale)
stage.event_key("up", jump_clash_royale)

def left_key(sprite):
    sprite.move_left(20)
    if sprite.get_x() < sprite.standing_on.left_side \
    and sprite.grounded:  
        sprite.set_gravity_on()
        sprite.grounded = False # new
        update_grounded_text()
sprite.event_key("left", left_key)

def right_key(sprite):
    sprite.move_right(20)
    if sprite.get_x() > sprite.standing_on.right_side \
    and sprite.grounded:
        sprite.set_gravity_on()
        sprite.grounded = False # new
        update_grounded_text()
sprite.event_key("right", right_key)
