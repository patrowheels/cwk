# ========== SETUP BACKGROUND ==========

stage.set_background("space")
stage.disable_all_walls()

# ========== SETUP SPRITES ==========

shuttle = codesters.Sprite("spaceshuttle", -210, 210)
shuttle.set_size(0.20)
shuttle.offset = 45
shuttle.set_rotation(0 + shuttle.offset)


ufo = codesters.Sprite("ufo")
ufo.set_size(0.25)


# ========== SETUP SHUTTLE CONTROLS ==========

def shuttle_up_batman():
    shuttle.move_up(20)
    shuttle.set_rotation(0 + shuttle.offset)
    

stage.event_key("w", shuttle_up_batman)

def shuttle_down_encanto():
    shuttle.move_down(20)
    shuttle.set_rotation(180 + shuttle.offset)
    
    
stage.event_key("s", shuttle_down_encanto)

def shuttle_left():
    shuttle.move_left(20)
    shuttle.set_rotation(90 + shuttle.offset)
    
stage.event_key("a", shuttle_left)

def shuttle_right():
    shuttle.move_right(20)
    shuttle.set_rotation(270 + shuttle.offset)

stage.event_key("d", shuttle_right)


# ========== SETUP UFO CONTROLS ==========
ufo.offset = 90

def ufo_up():
    ufo.move_up(20)
    ufo.set_rotation(0 + ufo.offset)

stage.event_key("up", ufo_up)

def ufo_down():
    ufo.move_down(20)
    ufo.set_rotation(180 + ufo.offset)

stage.event_key("down", ufo_down)

def ufo_left():
    ufo.move_left(20)
    ufo.set_rotation(90 + ufo.offset)

stage.event_key("left", ufo_left)

def ufo_right():
    ufo.move_right(20)
    ufo.set_rotation(270 + ufo.offset)

stage.event_key("right", ufo_right)

# ========== SETUP WALLS ==========

size = 175
for i in range(20):
    sprite = codesters.Rectangle(
        random.randint(-1 * size, size), # x-coordinate
        random.randint(-1 * size, size), # y-coordinate
        random.randint(10, 30), # width
        random.randint(10, 30), # height
        "darkred",
    )

while True:
    stage.wait(1)
    shot = codesters.Sprite("sun", 600, 600)
    shot.set_size(0.05)
    shot.set_x(ufo.get_x())
    shot.set_y(ufo.get_y())
    ufo_angle = ufo.get_rotation()
    print(ufo_angle)
    if ufo_angle == 0 or ufo_angle == 360:
        shot.set_x_speed(20)
