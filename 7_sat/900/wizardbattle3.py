# ========== SETUP STAGE ==========

stage.disable_all_walls()
stage.set_background("castle")

# ========== SETUP CHARACTERS ==========

fire_lady = codesters.Sprite("evilwitch", 200, 0)
fire_lady.health = 5
wizard = codesters.Sprite("wizard", -200, 0)
wizard.health = 5
wizard.flip_right_left()
characters = [fire_lady, wizard]

# ========== SETUP FIREBALL ==========

def make_fireball(sprite):
    fireball = codesters.Sprite("sun", 600, 600)
    fireball.set_size(.1)
    fireball.set_x(sprite.get_x() - 60)
    fireball.set_y(sprite.get_y() + 40)
    fireball.set_x_speed(-5)

fire_lady.event_key_press("space", make_fireball)

def make_iceball(sprite):
    iceball = codesters.Sprite("c6e7fdf106dc4dfead292824e17a33d2",
    600, 600)
    iceball.set_size(.1)
    iceball.set_x(sprite.get_x() + 40)
    iceball.set_y(sprite.get_y() + 70)
    iceball.set_x_speed(5)

wizard.event_key_press("a", make_iceball)

# ========== SETUP COLLISION ==========

def collision(person, attack):
    attack_type = attack.get_image_name() 
    if attack_type == "c6e7fdf106dc4dfead292824e17a33d2":
        person.health = person.health - 1
        person.say(person.health)
        stage.remove_sprite(attack)
        
        
        
    # add any other actions...
    
fire_lady.event_collision(collision)

while True:
    for speed in [-3, -2, -1, 0, 1, 2, 3, 2, 1, 0, -1, -2]:
        for person in characters:
            person.set_x_speed(speed)
            stage.wait(.1)

"""
* aiming system
* movement

"""
