player = codesters.Sprite("person2", -200, -150)
stage.set_gravity(10)
grounded = False

def left_key():
    player.move_left(20)
    # add other actions...
    
stage.event_key("left", left_key)

def right_key_pasta():
    player.move_right(20)
    # add other actions...         
    
stage.event_key("right", right_key_pasta)

def space_bar_chicken():
    global grounded
    if grounded == True:
        grounded = False
        player.jump(15)
        player.set_gravity_on()
    # add other actions...
    
stage.event_key("space", space_bar_chicken)

stage.set_bounce(0)

player.set_gravity_on()

floor = codesters.Rectangle(0, -350, 600, 250, "green") 
floor.set_gravity_off()

enemy = codesters.Sprite("ghost6", 200, 0)
enemy.set_size(.5)
enemy.set_x_speed(-1)


def collision(sprite, hit_sprite):
    global grounded
    if hit_sprite.get_color() == "green":
        grounded = True
        sprite.set_y_speed(0)
        sprite.set_gravity_off()
        sprite.set_y(sprite.get_y() + 5)
    elif hit_sprite.get_image_name() == "ghost6":
        sprite.say("ouchie!")
    # add any other actions...
    
player.event_collision(collision)

"""
game_running = True
while game_running == True:
    enemy.set_x(enemy.get_x() - 10)
    stage.wait(.2)
"""
