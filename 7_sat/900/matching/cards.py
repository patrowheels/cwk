class Card: # blueprint to make a "Card"
  def __init__(self, _x, _y): # when we create a new "Card"
    self.x = _x
    self.y = _y

  def print_pos(self): # Each Card knows how to print it's position
    print(
      f"The card is at {self.x}, {self.y}"
    )

cards = []
for x_pos in [100, 200]:
  for y_pos in [50, 150]:
    # code at this indentation level runs 4 times
    cards.append(
      Card(x_pos, y_pos) # Create new Cards by running line 2, the __init__
    )

# Afterwards, we have a list of four Cards
for card in cards: # Loop over the list
  card.print_pos() # Each Card knows how to do print_pos, which is on line 6
