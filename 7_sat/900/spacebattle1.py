# ========== SETUP BACKGROUND ==========

stage.set_background("space")

# ========== SETUP SPRITES ==========

shuttle = codesters.Sprite("spaceshuttle", -200, 200)
shuttle.set_size(0.20)
shuttle.offset = 90 - 315
shuttle.set_rotation(90 - shuttle.offset)

ufo = codesters.Sprite("ufo")
ufo.set_size(0.25)

# ========== SETUP CONTROLS ==========

def shuttle_up_batman():
    shuttle.move_up(20)

stage.event_key("w", shuttle_up_batman)

def shuttle_down_encanto():
    shuttle.move_down(20)
    
stage.event_key("s", shuttle_down_encanto)

# ========== SETUP WALLS ==========

sprite = codesters.Rectangle(
    random.randint(-5, 5), # x-coordinate
    random.randint(-5, 5), # y-coordinate
    random.randint(2, 4), # width
    random.randint(2, 4), # height
    "darkred",
)
