player = codesters.Sprite("robot", 200, 200)
positions = [
    (200, 200),
    (200, -200),
    (-200, -200),
    (-200, 200)
]
position_index = 0
stage.disable_all_walls()

def calc_vector(in_x, in_y):
    x_throw = in_x - player.get_x()
    y_throw = in_y - player.get_y()
    return [x_throw, y_throw]

def move_robot():
    global position_index
    position_index += 1
    if position_index == 4:
        position_index = 0
    player.set_x(positions[position_index][0])
    player.set_y(positions[position_index][1])

def throw():
    throw_vector = calc_vector(
        stage.click_x(),
        stage.click_y(),
    )
    snowball = codesters.Circle(player.get_x(), player.get_y(), 25, "gray")
    snowball.set_x_speed(throw_vector[0] * .05)
    snowball.set_y_speed(throw_vector[1] * .05)
    move_robot()


stage.event_click(throw)


