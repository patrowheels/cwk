# the list of possible words
possible_words = [
    "apple",
    "lasagna",
    "basketball",
    "coding",
    "country",
    "jazzfusion",
    ]

class HangmanWord:
    def __init__(self, word_param):
        # this runs when we create a new hangman word
        self.word = word_param
        self.presented_word = []
        for letter in self.word:
            self.presented_word.append('_')
        self.won = False
    
    def update_presented_word(self, in_guess):
        for i in range(len(self.word)):
            if in_guess == self.word[i]:
                self.presented_word[i] = in_guess
                
    def end_screen(self, text_sprite):
        if self.won == True:
            end_text = text_sprite.Text("YOU WIN!", 0, 0)
        else:
            end_text = text_sprite.Text("LOSE!", 0, 0)

# on the three lines below, we create a new HangmanWord
my_hangman = HangmanWord(
    random.choice(possible_words)
)

print(my_hangman.word)
print(my_hangman.presented_word)
my_hangman.update_presented_word("o")
print(my_hangman.presented_word)
my_hangman.update_presented_word("a")
print(my_hangman.presented_word)

"""
def end_screen(won):
    stage.clear()
    if won == True:
        end_text = codesters.Text("YOU WIN!", 0, 0)
    else:
        end_text = codesters.Text("LOSE!", 0, 0)
        

# choosing a random word from the list
word = random.choice(possible_words)

# making the underscore list
presented_word = []
for letter in word:
    presented_word.append('_')

finished = False # ADDED

guess_counter = 0

while finished == False: # CHANGED
    stage.clear()
    # making it as a presentable string
    presented_word_string = " ".join(presented_word)
    word_sprite = codesters.Text(presented_word_string, -100, 100)

    # let the user guess
    guess = stage.ask("Give me a letter", 0, 0)
    guess_counter += 1
    
    # check if letter is correct
    if guess in word:
        print("correct!")
        print(presented_word)
        for i in range(len(word)): # give all indices we can use
            if guess == word[i]: # compares that index in the word to the guessed letter
                presented_word[i] = guess # overwrite the underscores

    if "".join(presented_word) == word: # ADDED
        print("You won!") # ADDED
        print(f"It took you {guess_counter} guesses!") # ADDED
        finished = True # ADDED

stage.clear()
# making it as a presentable string
presented_word_string = " ".join(presented_word)
word_sprite = codesters.Text(presented_word_string, -100, 100)
stage.wait(2)
end_screen(won=True)
"""




