# ========== SETUP BACKGROUND ==========

stage.set_background("space")
stage.disable_all_walls()

# ========== SETUP SPRITES ==========

shuttle = codesters.Sprite("spaceshuttle", -210, 210)
shuttle.set_size(0.20)
shuttle.offset = 45
shuttle.set_rotation(0 + shuttle.offset)

ufo = codesters.Sprite("ufo")
ufo.set_size(0.25)


# ========== SETUP SHUTTLE CONTROLS ==========

def shuttle_up_batman():
    shuttle.move_up(20)
    shuttle.set_rotation(0 + shuttle.offset)
    

stage.event_key("w", shuttle_up_batman)

def shuttle_down_encanto():
    shuttle.move_down(20)
    shuttle.set_rotation(180 + shuttle.offset)
    
    
stage.event_key("s", shuttle_down_encanto)

def shuttle_left():
    shuttle.move_left(20)
    shuttle.set_rotation(90 + shuttle.offset)
    
stage.event_key("a", shuttle_left)

def shuttle_right():
    shuttle.move_right(20)
    shuttle.set_rotation(270 + shuttle.offset)

stage.event_key("d", shuttle_right)


# ========== SETUP UFO CONTROLS ==========
ufo.offset = 90

def ufo_up():
    ufo.move_up(20)
    ufo.set_rotation(0 + ufo.offset)

stage.event_key("up", ufo_up)

def ufo_down():
    ufo.move_down(20)
    ufo.set_rotation(180 + ufo.offset)

stage.event_key("down", ufo_down)

def ufo_left():
    ufo.move_left(20)
    ufo.set_rotation(90 + ufo.offset)

stage.event_key("left", ufo_left)

def ufo_right():
    ufo.move_right(20)
    ufo.set_rotation(270 + ufo.offset)

stage.event_key("right", ufo_right)

# ========== SETUP WALLS ==========

size = 175
for i in range(20):
    sprite = codesters.Rectangle(
        random.randint(-1 * size, size), # x-coordinate
        random.randint(-1 * size, size), # y-coordinate
        random.randint(10, 30), # width
        random.randint(10, 30), # height
        "darkred",
    )


def shot_collision(shot, other):
    my_var = other.get_color() 
    if my_var == "darkred":
        stage.remove_sprite(shot)
        shots.remove(shot)


shots = []
while True:
    stage.wait(1)
    
    # MAKE A NEW SHOT
    shot = codesters.Sprite("sun", 600, 600)
    shots.append(shot)
    shot.event_collision(shot_collision)
    shot.set_size(0.05)
    shot.set_x(ufo.get_x())
    shot.set_y(ufo.get_y())


            
    # REMOVE SHOTS THAT HAVE GONE TOO FAR
    for one_shot in shots:
        if abs(one_shot.get_x()) > 300 or \
        abs(one_shot.get_y()) > 300:
            shots.remove(shot)
            # stage.remove_sprite(shot)

    # SET THE SPEED FOR THE SHOT
    ufo_angle = ufo.get_rotation()
    if ufo_angle == 0 or ufo_angle == 360: # bottom down
        shot.set_y_speed(-20)
    elif ufo_angle == 90: # bottom pointing right
        shot.set_x_speed(20)
    elif ufo_angle == 180: # bottom up
        shot.set_y_speed(20)
    elif ufo_angle == 270: # bottom pointing left
        shot.set_x_speed(-20)

    # PRINT X/Y FOR EVERY SHOT
    for num, one_shot in enumerate(shots):
        print(
            num,
            one_shot.get_x(),
            one_shot.get_y(),
            )


"""
GOALS:
* Make the alien shoot the shuttle
* Make the shuttle fire something

STRETCH GOALS:
* Add powerups
"""


