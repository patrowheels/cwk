# Snowball Game



player = codesters.Sprite("robot", -200, -200)


def calc_vector(in_x, in_y):
    x_throw = in_x - player.get_x()
    y_throw = in_y - player.get_y()
    return [x_throw, y_throw]

def calc_magnitude(in_vector):
    return math.sqrt(in_vector[0] * in_vector[0] + in_vector[1] * in_vector[1])


def normalize(in_vector, in_mag):
    if in_mag == 0:
        return in_vector
    return [in_vector[0] / in_mag, in_vector[1] / in_mag]

def throw():
    
    throw_vector = calc_vector(
        stage.click_x(),
        stage.click_y(),
    )
    mag = calc_magnitude(throw_vector)
    
    normalized_vector = normalize(throw_vector, mag)
    snowball = codesters.Circle(-200, -200, 100, "gray")
    snowball.set_x_speed(normalized_vector[0])
    snowball.set_y_speed(normalized_vector[1])
    
    # player.say(f"I'm throwing towards {throw_vector[0]}, {throw_vector[1]}")
    player.say(f"The click was {round(mag)} pixels away")
stage.event_click(throw)
