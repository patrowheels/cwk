# Goals
## Main Goals
* Make robot 1 & 2 move and fire lasers

## Stretch Goals
* Make an end screen
* Make obstacles
* Limit laser fire rate
  * Make a "can fire" lock

## Advanced Goals
* Make a start screen/start button
* Make health bars over the robots
* Make powerups
* Make multiple stages
  * Make the stage randomly selected

## Super Advanced Goals
* Make a stage select screen