# PRESENTATIONS
* Time: 11:15-11:45 PST, 2:15 EST
* Instructions		
  1. Choose some code you've written in this class
  2. Add comments
  3. Prepare to explain how the code works
  4. Demonstrate the project, and explain the code to your parents and the class
 * 	Presentations should be 3-5 minutes to ensure that we have enough time for everyone. If someone takes too long, I might cut them off.
  
  
## Stretch Goal
* While you are presenting, change/comment out lines of code