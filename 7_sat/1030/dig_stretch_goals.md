# Stretch Goals

* Create beginning screen
* Create game over screen
* Make it so that the mouse destroys the treasure
* Add ladders
* Create a code block that repositions the mouse, that is better than "go to random position"

On higher levels:
  * Make multiple enemies
  * Make a new type of enemy
  * Make powerups that change the player

## Very Difficult Goal

* Make it so that when you walk past a tunnel you've already made, you fall down that tunnel
