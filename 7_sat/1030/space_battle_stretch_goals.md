# Stretch Goals
* Make the alien
  * Make the alien move up and down randomly
  * Make the alien fire randomly or fire at the players
* Make different versions of the game
  * Make the game co-op vs. enemy, or player vs. player vs. enemy
* Powerups
  * Get a powerup to shoot faster, or shoot a different weapon
* Health bars