sprites_shown = 0

sprite_list = []
for i in range(2):
    for sprite_name in "applecore football bat baseball".split():
        sprite_list.append(
            codesters.Sprite(
                sprite_name,
                500, # x
                500, # y
            )
        )

for one_sprite in sprite_list:
    one_sprite.set_size(.6)

random.shuffle(sprite_list)

x_width = 150
x_step = 100
location_list = []
for x_pos in range(-1 * x_width, 1 + x_width, x_step):
    for y_pos in range(
        100, # start
        201, # stop (one more than stopping position)
        100, # step
    ):
        location_list.append((x_pos, y_pos))

for i in range(len(sprite_list)):
    sprite_list[i].set_x(location_list[i][0])
    sprite_list[i].set_y(location_list[i][1])


card_list = []
for x, y in location_list:
    card_list.append(
        codesters.Rectangle(
            x,
            y,
            80,
            80,
            "darkgreen",
        )
    )

def click(sprite):
    global sprites_shown
    if sprites_shown < 2:
        sprite.hide()
        sprites_shown += 1
        if sprites_shown == 2:
            stage.wait(1)
            for one_card in card_list:
                one_card.show()
            sprites_shown = 0

for one_card in card_list:
    one_card.event_click(click)


