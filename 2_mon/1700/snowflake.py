print(20 * '=')

# ==================== SETUP STAGE ====================

stage.disable_all_walls()

stage.set_background("winter")

def make_snowflake():
    flakes = [         
        "snowflake1",         
        "snowflake2",          
        "snowflake3",         
        ]          
    new_flake = codesters.Sprite(
        random.choice(flakes), 
        350, # x
        350, # y
    )
    new_flake.set_size(
        0.1 * random.randint(4, 6)# CHANGED
    )
    new_flake.set_rotation( # NEW
        random.randint(0, 359) # NEW 
    ) # NEW
    new_flake.go_to(
        random.randint(-225, 225), # x
        250, # y
    )
    new_flake.set_y_speed(-3)
    
    
while True:
    stage.wait(.1) # CHANGED
    make_snowflake()
    
"""
GOALS:
  * use random.choice to randomly make one of 3 different types of
  snowflakes: "snowflake1", "snowflake2", "snowflake3"
  * randomly rotate the snowflakes
  * make a snowman that can move left/right
  * make the snowflakes move semi-random, but move towards
  the snowman
"""
