hero = codesters.Sprite(
    "costumed_person1", # image name
    -175, # x
    -175, # y
)
hero.set_size(.4) # shrink it by half

villain = codesters.Sprite(
    "709bea192d0e4c09b8cba6c9536abff9", # image name
    175, # x
    175, # y
)
villain.set_size(.4)

def left_key(sprite):
    sprite.move_left(20)

hero.event_key("left", left_key)

def right_key(sprite):
    sprite.move_right(20)

hero.event_key("right", right_key)

def up_key(sprite):
    sprite.move_up(20)

hero.event_key("up", up_key)

def down_key(sprite):
    sprite.move_down(20)

hero.event_key("down", down_key)

stage.disable_all_walls()

def normalize(in_x, in_y):
    hyp = math.sqrt(
        in_x**2 + in_y**2
    )
    return in_x/hyp, in_y/hyp

def hero_throw():
    x_click = (stage.click_x() - hero.get_x()) * 0.1 # CHANGED
    y_click = (stage.click_y() - hero.get_y()) * 0.1 # CHANGED
    snowball = codesters.Circle(
        hero.get_x(), # x # CHANGED
        hero.get_y(), # y # CHANGED
        15, # diameter
        "white", # color
    )
    snowball.set_line_thickness(2)
    snowball.set_outline_color("black")
    x_click, y_click = normalize(x_click, y_click)
    snowball.set_x_speed(x_click * 10)
    snowball.set_y_speed(y_click * 10)

stage.event_click(hero_throw)

def villain_throw():
    snowball = codesters.Circle(
        villain.get_x(), # x # CHANGED
        villain.get_y(), # y # CHANGED
        15, # diameter
        "white", # color
    )
    snowball.set_line_thickness(2)
    snowball.set_outline_color("black")
    snowball.set_x_speed(
        (hero.get_x() - villain.get_x()) * 0.05    
    )
    snowball.set_y_speed(
        (hero.get_y() - villain.get_y()) * 0.05
    )

while True:
    stage.wait(1)
    villain_throw()
