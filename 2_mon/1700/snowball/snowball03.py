ters.Sprite(
    "costumed_person1", # image name
    -175, # x
    -175, # y
)
hero.set_size(.4) # shrink it by half

villain = codesters.Sprite(
    "709bea192d0e4c09b8cba6c9536abff9", # image name
    175, # x
    175, # y
)
villain.set_size(.4)

def left_key(sprite):
    sprite.move_left(20)
    # add other actions...
    
hero.event_key("left", left_key)

def right_key(sprite):
    sprite.move_right(20)
    # add other actions...
    
hero.event_key("right", right_key)

def up_key(sprite):
    sprite.move_up(20)
    # add other actions...
    
hero.event_key("up", up_key)

def down_key(sprite):
    sprite.move_down(20)
    # add other actions...
    
hero.event_key("down", down_key)

stage.disable_all_walls()

def hero_throw():
    x_click = stage.click_x()
    y_click = stage.click_y()
    snowball = codesters.Circle(
        0, # x
        0, # y
        15, # diameter
        "white", # color
    )
    snowball.set_line_thickness(2)
    snowball.set_outline_color("black")
    snowball.set_x_speed(x_click)
    snowball.set_y_speed(y_click)

stage.event_click(hero_throw)


