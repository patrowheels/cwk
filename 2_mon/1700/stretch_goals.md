# Brick Breaker goals
## Main goals
* Create a paddle
* Create a game over screen
  * Make the game end when the ball hits the bottom of the screen

## Advanced goals

* Make lots of bricks
* Make different rows have different color bricks
  * Make the different color bricks behave differently
    * Some could be worth more points
    * Some could speed up the ball
    * Some bricks could provide an extra ball
* Let the paddle shoot lasers

