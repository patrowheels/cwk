score = 0

# ===== SETUP TEXT =====

score_text = codesters.Text(
    f"Score: {score}",
    -200, # x pos
    200, # y pos
)

# ===== SETUP BRICKS =====

for y_location in [-50, 50, 150]:
    for x_location in [-150, 0, 150]:
        new_rec = codesters.Rectangle(
                    x_location, # x pos
                    y_location, # y pos
                    100, # width
                    50, # height
                    "darkred"
                )
        new_rec.set_outline_color("black")
        new_rec.set_line_thickness(5)

# ===== SETUP BALL =====

ball = codesters.Circle(
    0, # x pos 
    -150, # y pos
    30, # radius (size)
    "coral" # color
)
ball.set_outline_color("black")
ball.set_line_thickness(2)

ball.set_x_speed(2)
ball.set_y_speed(1)

