core = 0
lives = 5

# ===== SETUP TEXT =====

score_text = codesters.Text(
    f"Score: {score}",
    -200, # x pos
    200, # y pos
)

# new
lives_text = codesters.Text(
    f"Lives: {lives}",
    -200, # x pos
    220, # y pos
)

# ===== SETUP FLOOR =====

# new
floor = codesters.Rectangle(
    50, # x
    -250, # y
    600, # width
    50, # height
    "gray",
)

# ===== SETUP BRICKS =====

recs = []
for y_location in [-50, 50, 150]:
    for x_location in [-150, 0, 150]:
        new_rec = codesters.Rectangle(
                    x_location, # x pos
                    y_location, # y pos
                    100, # width
                    50, # height
                    "darkred"
                )
        new_rec.set_outline_color("black")
        new_rec.set_line_thickness(5)
        recs.append(new_rec)

# ===== SETUP BALL =====

ball = codesters.Circle(
    0, # x pos 
    -150, # y pos
    30, # radius (size)
    "coral" # color
)
ball.set_outline_color("black")
ball.set_line_thickness(2)

ball.set_x_speed(10)
ball.set_y_speed(5)


# ===== SETUP END =====

def end_game():
    codesters.Text("End!!!!")
    for one_rec in recs:
        stage.remove_sprite(one_rec)

# ===== SETUP COLLISION =====


def collision_thor(sprite, hit_sprite):
    global score
    global lives
    my_color = hit_sprite.get_color() 
    if my_color == "darkred":
        stage.remove_sprite(hit_sprite)
        score = score + 1
        score_text.set_text(f"Score: {score}")
        sprite.set_y_speed(
            sprite.get_y_speed() * -1
        )
    if my_color == "blue":
        sprite.set_y_speed(
            sprite.get_y_speed() * -1
        )
    if my_color == "gray":
        lives = lives - 1
        lives_text.set_text(f"Lives: {lives}")
        sprite.set_y_speed(
            sprite.get_y_speed() * -1
        )
        if lives <= 0:
            end_game()
    
ball.event_collision(collision_thor)

# ===== SETUP PADDLE =====

paddle = codesters.Rectangle(
    30, # x
    -180, # y
    120, # width
    20, # height
    "blue", # color
)


def left_key_hulk(sprite_iron_man):
    sprite_iron_man.move_left(20)
    # add other actions...
    
paddle.event_key("left", left_key_hulk)

def right_key(sprite):
    sprite.move_right(20)
    # add other actions...
    
paddle.event_key("right", right_key)


