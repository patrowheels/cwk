PLAYER:

package space.codekingdoms.alexteacher8.race;

import com.codekingdoms.nozzle.base.BasePlayer;

public class Player extends BasePlayer {
	public void onRunCommand(String command, String[] args) {
		if (command.equals("/start")) {
			getGame().currPlayer = this;
			getGame().startLocation = getLocation();
			getGame().startTimer(5);
		}

	}
	
}

GAME:

package space.codekingdoms.alexteacher8.race;
import com.codekingdoms.nozzle.base.BaseGame;
import org.bukkit.Location;

public class Game extends BaseGame {
	
	public Location startLocation;
	
	public Player currPlayer;
	
	public void onTimerExpire() {
		Location endLocation = currPlayer.getLocation();
		double dist = endLocation.distance(startLocation);
		currPlayer.sendMessage("Distance: " + dist);
	}
}
