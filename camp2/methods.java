package space.codekingdoms.alexteacher8.methods;

import com.codekingdoms.nozzle.base.BasePlayer;
import org.bukkit.Material;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;

public class Player extends BasePlayer {

	public void onRunCommand(String command, String[] args) {
		if (command.equals("/clear")) {
			getGame().removeAllMobs();
		}
	}
	
	public void startup() {
		
		clearInventory();
		addItemToInventory(Material.TORCH);
		addItemToInventory(Material.STONE_PICKAXE);
	
	}
	
	public void onJoin() {
		
		startup();
	
	}
	
	public void onRespawn() {
		
		startup();
	
	}
	
	public void changeBlock( Material newMaterial ) {
		
		getGame().setBlockTypeAtLocation(newMaterial, getTargetBlock().getLocation());
	
	}
	
	public void onLeftClick() {
		
		changeBlock(Material.OAK_LEAVES);
		Panda p = new Panda(); // change
		p.spawn(world, getLocation()); // change
	
	}
	
	public int countCreatures( EntityType type ) {
		
		int count = 0;
		for (Entity e : world.getNearbyEntities(getLocation(), 5, 5, 5)) {
			
			if (e.getType() == type) {
				
				count = count + 1;
				
			}
			
			
		}
		
		return count;
	
	}
	
	public void onRightClick() {
		
		sendMessage("There are " + (countCreatures(EntityType.PANDA) + " pandas nearby")); // change
	
	}
	
	
}
