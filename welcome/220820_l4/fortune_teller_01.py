import random

responses = """
yes
no
maybe
""".strip().split('\n')

# strip changes the string
# "\nyes\nno\nmaybe\n" -> "yes\nno\nmabye"
# split breaks it up into pieces
# "yes\nno\nmaybe".split('\n') -> ['yes', 'no', 'maybe']

user_question = input("Ask a question:\n> ")

print(
  random.choice(
    responses
  )
)
