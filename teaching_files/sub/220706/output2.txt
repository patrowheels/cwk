```python3
iff --git a/home/atb-teaching/cwk/teaching_files/sub/220706/scroller1.py b/home/atb-teaching/cwk/teaching_files/sub/220706/scroller10.py
ndex 0716f40..24d90de 100644
-- a/home/atb-teaching/cwk/teaching_files/sub/220706/scroller1.py
++ b/home/atb-teaching/cwk/teaching_files/sub/220706/scroller10.py
@ -10,6 +10,19 @@ hero.set_gravity_on()
hero.set_size(.5) # changed
hero.jumps = 2
hero.alive = True
hero.health = 5          # added line
hero.invincible = False          # added line

          # added line
# ===== SETUP HEALTH TEXT =====          # added line

          # added line
health_text = codesters.Text(          # added line
        f"Health: {hero.health}",          # added line
        -175,          # added line
        175          # added line
)          # added line

          # added line
def update_health_text():          # added line
    health_text.set_text(f"Health: {hero.health}")          # added line

# ===== SETUP JUMP =====

@ -22,6 +35,27 @@ def space_bar_roblox(): # function/set of instructions

stage.event_key("space", space_bar_roblox)

# ===== SETUP MOVEMENT =====          # added line

          # added line
def drop():          # added line
    if hero.get_y_speed != 0:          # added line
        hero.set_y_speed(-20)          # added line

          # added line
stage.event_key("down", drop)          # added line

          # added line
def right_key(sprite):          # added line
    if sprite.get_x() < -150:          # added line
        sprite.move_right(20)          # added line

          # added line
hero.event_key("right", right_key)          # added line

          # added line
def left_key(sprite):          # added line
    if sprite.get_x() > -250:          # added line
        sprite.move_left(20)          # added line

          # added line
hero.event_key("left", left_key)          # added line

          # added line

          # added line
# ===== SETUP GROUND =====

ground = codesters.Rectangle(
@ -48,21 +82,30 @@ def collision_harry_potter(sprite, hit_sprite):
        )
        hero.jumps = 2
        hero.say(hero.jumps)

          # added line
    if my_var == "blue" and not hero.invincible:          # added line
        hero.invincible = True          # added line
        hero.health = hero.health - 1          # added line
        update_health_text()          # added line
        if hero.health <= 0:          # added line
            hero.alive = False          # added line
        stage.wait(2)          # added line
        hero.invincible = False           # added line
        
hero.event_collision(collision_harry_potter)

# ===== SETUP OBSTACLE =====

counter = 0          # added line
while hero.alive:
    obstacle = codesters.Rectangle(150, -175, 50, 50, "blue")     # deleted line
    obstacle.set_gravity_off()     # deleted line
    obstacle.set_x_speed(-5)     # deleted line
    stage.wait(2)     # deleted line

     # deleted line
"""     # deleted line
height = 50     # deleted line
obstacle = codesters.Rectangle(150,      # deleted line
    ground.get_y() + ground.get_height()/2 + height/2     # deleted line
    , 50, height, "blue")     # deleted line
obstacle.set_gravity_off()     # deleted line
"""     # deleted line
    counter = counter + 1          # added line
    if counter % 5 == 0:          # added line
        height = random.randint(0, 7) * -25          # added line
        obstacle = codesters.Rectangle(150, height, 50, 50, "blue")          # added line
        obstacle.set_gravity_off()          # added line
        obstacle.set_x_speed(-5)          # added line
    stage.wait(.1)          # added line

          # added line
end_text = codesters.Text("End")          # added line
for one_sprite in [hero, ground, health_text]:          # added line
    stage.remove_sprite(one_sprite)          # added line
```
```python3
iff --git a/home/atb-teaching/cwk/teaching_files/sub/220706/scroller10.py b/home/atb-teaching/cwk/teaching_files/sub/220706/scroller11.py
ndex 24d90de..3b10c75 100644
-- a/home/atb-teaching/cwk/teaching_files/sub/220706/scroller10.py
++ b/home/atb-teaching/cwk/teaching_files/sub/220706/scroller11.py
@ -12,6 +12,7 @@ hero.jumps = 2
hero.alive = True
hero.health = 5
hero.invincible = False
hero.jump_lock = False          # added line

# ===== SETUP HEALTH TEXT =====

@ -27,7 +28,8 @@ def update_health_text():
# ===== SETUP JUMP =====

def space_bar_roblox(): # function/set of instructions
    if hero.jumps > 0:     # deleted line
    if hero.jumps > 0 and not hero.jump_lock:          # added line
        hero.jump_lock = True          # added line
        hero.jumps = hero.jumps - 1
        hero.say(hero.jumps)
        hero.jump(15)
@ -35,6 +37,11 @@ def space_bar_roblox(): # function/set of instructions

stage.event_key("space", space_bar_roblox)

def release_key(sprite):          # added line
    sprite.jump_lock = False          # added line
              # added line
hero.event_key_release("space", release_key)          # added line

          # added line
# ===== SETUP MOVEMENT =====

def drop():
```
```python3
iff --git a/home/atb-teaching/cwk/teaching_files/sub/220706/scroller11.py b/home/atb-teaching/cwk/teaching_files/sub/220706/scroller2.py
ndex 3b10c75..0716f40 100644
-- a/home/atb-teaching/cwk/teaching_files/sub/220706/scroller11.py
++ b/home/atb-teaching/cwk/teaching_files/sub/220706/scroller2.py
@ -10,26 +10,11 @@ hero.set_gravity_on()
hero.set_size(.5) # changed
hero.jumps = 2
hero.alive = True
hero.health = 5     # deleted line
hero.invincible = False     # deleted line
hero.jump_lock = False     # deleted line

     # deleted line
# ===== SETUP HEALTH TEXT =====     # deleted line

     # deleted line
health_text = codesters.Text(     # deleted line
        f"Health: {hero.health}",     # deleted line
        -175,     # deleted line
        175     # deleted line
)     # deleted line

     # deleted line
def update_health_text():     # deleted line
    health_text.set_text(f"Health: {hero.health}")     # deleted line

# ===== SETUP JUMP =====

def space_bar_roblox(): # function/set of instructions
    if hero.jumps > 0 and not hero.jump_lock:     # deleted line
        hero.jump_lock = True     # deleted line
    if hero.jumps > 0:          # added line
        hero.jumps = hero.jumps - 1
        hero.say(hero.jumps)
        hero.jump(15)
@ -37,32 +22,6 @@ def space_bar_roblox(): # function/set of instructions

stage.event_key("space", space_bar_roblox)

def release_key(sprite):     # deleted line
    sprite.jump_lock = False     # deleted line
         # deleted line
hero.event_key_release("space", release_key)     # deleted line

     # deleted line
# ===== SETUP MOVEMENT =====     # deleted line

     # deleted line
def drop():     # deleted line
    if hero.get_y_speed != 0:     # deleted line
        hero.set_y_speed(-20)     # deleted line

     # deleted line
stage.event_key("down", drop)     # deleted line

     # deleted line
def right_key(sprite):     # deleted line
    if sprite.get_x() < -150:     # deleted line
        sprite.move_right(20)     # deleted line

     # deleted line
hero.event_key("right", right_key)     # deleted line

     # deleted line
def left_key(sprite):     # deleted line
    if sprite.get_x() > -250:     # deleted line
        sprite.move_left(20)     # deleted line

     # deleted line
hero.event_key("left", left_key)     # deleted line

     # deleted line

     # deleted line
# ===== SETUP GROUND =====

ground = codesters.Rectangle(
@ -89,30 +48,21 @@ def collision_harry_potter(sprite, hit_sprite):
        )
        hero.jumps = 2
        hero.say(hero.jumps)

     # deleted line
    if my_var == "blue" and not hero.invincible:     # deleted line
        hero.invincible = True     # deleted line
        hero.health = hero.health - 1     # deleted line
        update_health_text()     # deleted line
        if hero.health <= 0:     # deleted line
            hero.alive = False     # deleted line
        stage.wait(2)     # deleted line
        hero.invincible = False      # deleted line
        
hero.event_collision(collision_harry_potter)

# ===== SETUP OBSTACLE =====

counter = 0     # deleted line
while hero.alive:
    counter = counter + 1     # deleted line
    if counter % 5 == 0:     # deleted line
        height = random.randint(0, 7) * -25     # deleted line
        obstacle = codesters.Rectangle(150, height, 50, 50, "blue")     # deleted line
        obstacle.set_gravity_off()     # deleted line
        obstacle.set_x_speed(-5)     # deleted line
    stage.wait(.1)     # deleted line

     # deleted line
end_text = codesters.Text("End")     # deleted line
for one_sprite in [hero, ground, health_text]:     # deleted line
    stage.remove_sprite(one_sprite)     # deleted line
    obstacle = codesters.Rectangle(150, -175, 50, 50, "blue")          # added line
    obstacle.set_gravity_off()          # added line
    obstacle.set_x_speed(-5)          # added line
    stage.wait(2)          # added line

          # added line
"""          # added line
height = 50          # added line
obstacle = codesters.Rectangle(150,           # added line
    ground.get_y() + ground.get_height()/2 + height/2          # added line
    , 50, height, "blue")          # added line
obstacle.set_gravity_off()          # added line
"""          # added line
```
```python3
iff --git a/home/atb-teaching/cwk/teaching_files/sub/220706/scroller2.py b/home/atb-teaching/cwk/teaching_files/sub/220706/scroller3.py
ndex 0716f40..6474098 100644
-- a/home/atb-teaching/cwk/teaching_files/sub/220706/scroller2.py
++ b/home/atb-teaching/cwk/teaching_files/sub/220706/scroller3.py
@ -58,11 +58,3 @@ while hero.alive:
    obstacle.set_gravity_off()
    obstacle.set_x_speed(-5)
    stage.wait(2)

     # deleted line
"""     # deleted line
height = 50     # deleted line
obstacle = codesters.Rectangle(150,      # deleted line
    ground.get_y() + ground.get_height()/2 + height/2     # deleted line
    , 50, height, "blue")     # deleted line
obstacle.set_gravity_off()     # deleted line
"""     # deleted line
```
```python3
iff --git a/home/atb-teaching/cwk/teaching_files/sub/220706/scroller3.py b/home/atb-teaching/cwk/teaching_files/sub/220706/scroller4.py
ndex 6474098..a3642d9 100644
-- a/home/atb-teaching/cwk/teaching_files/sub/220706/scroller3.py
++ b/home/atb-teaching/cwk/teaching_files/sub/220706/scroller4.py
@ -53,8 +53,11 @@ hero.event_collision(collision_harry_potter)

# ===== SETUP OBSTACLE =====

counter = 0          # added line
while hero.alive:
    obstacle = codesters.Rectangle(150, -175, 50, 50, "blue")     # deleted line
    obstacle.set_gravity_off()     # deleted line
    obstacle.set_x_speed(-5)     # deleted line
    stage.wait(2)     # deleted line
    counter = counter + 1          # added line
    if counter % 20 == 0:          # added line
        obstacle = codesters.Rectangle(150, -175, 50, 50, "blue")          # added line
        obstacle.set_gravity_off()          # added line
        obstacle.set_x_speed(-5)          # added line
    stage.wait(.1)          # added line
```
```python3
iff --git a/home/atb-teaching/cwk/teaching_files/sub/220706/scroller4.py b/home/atb-teaching/cwk/teaching_files/sub/220706/scroller5.py
ndex a3642d9..af26926 100644
-- a/home/atb-teaching/cwk/teaching_files/sub/220706/scroller4.py
++ b/home/atb-teaching/cwk/teaching_files/sub/220706/scroller5.py
@ -10,6 +10,18 @@ hero.set_gravity_on()
hero.set_size(.5) # changed
hero.jumps = 2
hero.alive = True
hero.health = 5          # added line

          # added line
# ===== SETUP HEALTH TEXT =====          # added line

          # added line
health_text = codesters.Text(          # added line
        f"Health: {hero.health}",          # added line
        -175,          # added line
        175          # added line
)          # added line

          # added line
def update_health_text():          # added line
    health_text.set_text(f"Health: {hero.health}")          # added line

# ===== SETUP JUMP =====

@ -48,6 +60,10 @@ def collision_harry_potter(sprite, hit_sprite):
        )
        hero.jumps = 2
        hero.say(hero.jumps)

          # added line
    if my_var == "blue":          # added line
        hero.health = hero.health - 1          # added line
        update_health_text()          # added line
        
hero.event_collision(collision_harry_potter)

```
```python3
iff --git a/home/atb-teaching/cwk/teaching_files/sub/220706/scroller5.py b/home/atb-teaching/cwk/teaching_files/sub/220706/scroller6.py
ndex af26926..a22c441 100644
-- a/home/atb-teaching/cwk/teaching_files/sub/220706/scroller5.py
++ b/home/atb-teaching/cwk/teaching_files/sub/220706/scroller6.py
@ -11,6 +11,7 @@ hero.set_size(.5) # changed
hero.jumps = 2
hero.alive = True
hero.health = 5
hero.invincible = False          # added line

# ===== SETUP HEALTH TEXT =====

@ -61,9 +62,12 @@ def collision_harry_potter(sprite, hit_sprite):
        hero.jumps = 2
        hero.say(hero.jumps)

    if my_var == "blue":     # deleted line
    if my_var == "blue" and not hero.invincible:          # added line
        hero.invincible = True          # added line
        hero.health = hero.health - 1
        update_health_text()
        stage.wait(2)          # added line
        hero.invincible = False           # added line
        
hero.event_collision(collision_harry_potter)

```
```python3
iff --git a/home/atb-teaching/cwk/teaching_files/sub/220706/scroller6.py b/home/atb-teaching/cwk/teaching_files/sub/220706/scroller7.py
ndex a22c441..267d7fe 100644
-- a/home/atb-teaching/cwk/teaching_files/sub/220706/scroller6.py
++ b/home/atb-teaching/cwk/teaching_files/sub/220706/scroller7.py
@ -66,6 +66,8 @@ def collision_harry_potter(sprite, hit_sprite):
        hero.invincible = True
        hero.health = hero.health - 1
        update_health_text()
        if hero.health <= 0:          # added line
            hero.alive = False          # added line
        stage.wait(2)
        hero.invincible = False 
        
@ -81,3 +83,5 @@ while hero.alive:
        obstacle.set_gravity_off()
        obstacle.set_x_speed(-5)
    stage.wait(.1)

          # added line
end_text = codesters.Text("End")          # added line
```
```python3
iff --git a/home/atb-teaching/cwk/teaching_files/sub/220706/scroller7.py b/home/atb-teaching/cwk/teaching_files/sub/220706/scroller8.py
ndex 267d7fe..7370b01 100644
-- a/home/atb-teaching/cwk/teaching_files/sub/220706/scroller7.py
++ b/home/atb-teaching/cwk/teaching_files/sub/220706/scroller8.py
@ -85,3 +85,5 @@ while hero.alive:
    stage.wait(.1)

end_text = codesters.Text("End")
for one_sprite in [hero, ground, health_text]:          # added line
    stage.remove_sprite(one_sprite)          # added line
```
```python3
iff --git a/home/atb-teaching/cwk/teaching_files/sub/220706/scroller8.py b/home/atb-teaching/cwk/teaching_files/sub/220706/scroller9.py
ndex 7370b01..392bb52 100644
-- a/home/atb-teaching/cwk/teaching_files/sub/220706/scroller8.py
++ b/home/atb-teaching/cwk/teaching_files/sub/220706/scroller9.py
@ -35,6 +35,27 @@ def space_bar_roblox(): # function/set of instructions

stage.event_key("space", space_bar_roblox)

# ===== SETUP MOVEMENT =====          # added line

          # added line
def drop():          # added line
    if hero.get_y_speed != 0:          # added line
        hero.set_y_speed(-20)          # added line

          # added line
stage.event_key("down", drop)          # added line

          # added line
def right_key(sprite):          # added line
    if sprite.get_x() < -150:          # added line
        sprite.move_right(20)          # added line

          # added line
hero.event_key("right", right_key)          # added line

          # added line
def left_key(sprite):          # added line
    if sprite.get_x() > -250:          # added line
        sprite.move_left(20)          # added line

          # added line
hero.event_key("left", left_key)          # added line

          # added line

          # added line
# ===== SETUP GROUND =====

ground = codesters.Rectangle(
```
