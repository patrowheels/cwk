# Sub Guide

## Intro

We've been working on brick breaker using replit and codesters. The files are [here](/3_tues/1530/). We made a class `Brick` during one lesson, but I didn't explain it thoroughly, I just said "this is like when we make things in Codesters, usually Codesters gives us the blueprint." The replit is [here](https://replit.com/@AlBurns/tues-1530?v=1). I've been going pretty slowly, so don't feel rushed!

## Presentations

Since I've only had one class with each student, and since I'm a pretty slow teacher, the students won't have much to present. You can have them present whatever they want, or you can put off presentations until next week. Anything is fine.

## Making the file

Start with [brick_breaker2.py](brick_breaker2.py). It's totally fine if they directly copy this file if they're behind.

I showed them a loop-in-a-loop last time, but that'd be good to review. You can review the files [loop_counter.py](/3_tues/1530/brick_breaker/loop_counter.py) and [brick_maker.py](/3_tues/1530/brick_breaker/make_bricks.py) for that.

```python3
bricks = []
for y_pos in [200, 180, 160, 140]:
    for x_pos in [-200, -150, -100, -50, 0, 50, 100, 150, 200]:
        bricks.append(codesters.Rectangle(
            x_pos,# x pos
            y_pos,# y pos
            45,# width
            15,# height
            "darkred",
        ))

ball = codesters.Circle(
        3,    # x
        -100, # y
        25,   # diameter
        "black",
)

paddle = codesters.Rectangle(
    # x
    # y
    # width
    # height
    # color
)
```
Next, [brick_breaker3.py](brick_breaker3.py).

Here we setup the paddle.

What happens if we give the paddle 500 width?

What happens if we set the y to 225?

```python3
ball = codesters.Circle(
)
 
paddle = codesters.Rectangle(
    # x     # deleted line
    # y     # deleted line
    # width     # deleted line
    # height     # deleted line
    # color     # deleted line
    0, # x          # added line
    -150, # y          # added line
    100,# width          # added line
    15, # height          # added line
    "yellow", # color          # added line
)
paddle.set_outline_color("black")          # added line
```

Next, [brick_breaker4.py](brick_breaker4.py).

I added some comments to demonstrate sections, and we add movement.

I usually like to give a suffix to the functions that are arguments. Something like `left_key_minions`.

Ask what happens when you change the numbers from 20 to 10 or 30.

Ask what happens when you replace `left_key` with `right_key`. What does each button do?

```python3
# ===== SETUP MOVEMENT =====          # added line

def left_key(sprite):          # added line
    sprite.move_left(20)          # added line

paddle.event_key("left", left_key)          # added line

def right_key(sprite):          # added line
    sprite.move_right(20)          # added line

paddle.event_key("right", right_key)          # added line
```

Next, [brick_breaker5.py](brick_breaker5.py).

To better understand `random.randint`, you can make [addition_tester.py](addition_tester.py).

We give the ball a random x/y speed.

Can the ball have no speed? is this a bug? (yes, but I don't wanna worry about that)

What happens if we change the numbers in `random.randint()`?

```python3
ball = codesters.Circle(
        "black",
)
 
ball.set_x_speed(          # added line
    random.randint(-2, 2)          # added line
)          # added line

ball.set_y_speed(          # added line
    random.randint(-2, 2)          # added line
)          # added line

# ===== SETUP PADDLE =====
 
paddle = codesters.Rectangle(
```

Next, [brick_breaker6.py](brick_breaker6.py).

There's a lot here. Try to explain how a bounce is just swapping x/y speed to -1 * that speed.

![bounce](bounce.png)

```python3
paddle.event_key("right", right_key)

# ===== SETUP COLLISION =====          # added line

def collision(main_ball, other_shape):          # added line
    other_shape_color = other_shape.get_color()           # added line
    if other_shape_color == "darkred":          # added line
        main_ball.set_y_speed(          # added line
            main_ball.get_y_speed() * -1          # added line
        )          # added line
        bricks.remove(other_shape)          # added line
        stage.remove_sprite(other_shape)          # added line
    if other_shape_color == "yellow":          # added line
        main_ball.set_y_speed(          # added line
            main_ball.get_y_speed() * -1          # added line
        )          # added line
                  # added line
ball.event_collision(collision)          # added line
```

Next, [brick_breaker7.py](brick_breaker7.py).

Talk about the length of the list `bricks`, and how it changes. What would happen if you changed the `0`, so instead it said `if len(bricks) == 30:`?

```python3
def collision(main_ball, other_shape):
        )
        bricks.remove(other_shape)
        stage.remove_sprite(other_shape)
        if len(bricks) == 0:          # added line
            end_game()          # added line
    if other_shape_color == "yellow":
        main_ball.set_y_speed(
            main_ball.get_y_speed() * -1
        )
         
ball.event_collision(collision)

# ===== SETUP GAME END =====          # added line

def end_game():          # added line
    stage.remove_sprite(paddle)          # added line
    stage.remove_sprite(ball)          # added line
    end_text = codesters.Text("You win!")          # added line
```