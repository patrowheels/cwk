We will start by adding a method to our class, which is like adding a recipe to our recipe book.

![add method](g1106.png)

We want our method to run whenever the player types something in the chat, so we will select `onchat`.

![on chat](g1110.png)

We can test to make sure they typed the right thing in chat. If they did type the right thing, we can continue with our recipe steps.

![on chat](g1114.png)

Here, we choose what we want to look for before we continue following our recipe.

![on chat](g1118.png)

We select `build a block`, because we need to make something that is kind of big.

![on chat](g1122.png)

We are going to examine the `message` variable, which points at whatever the user typed in.

![on chat](g1126.png)

We are going to check if that `message` variable `equals` the word that we want to check for. You can search for `equals` and click on it.

![on chat](g1131.png)

We can't add more stuff in this menu, so click the `place` button.

![on chat](g1135.png)

Now we can add what we want `message` to equal.

![on chat](g1139.png)

`String` means a bunch of letters, numbers, and punctuation that you want the computer to remember. We can check whatever the user types against the string `"makestone"`.

![on chat](g1143.png)

Type in `makestone` and click the green check mark.

![on chat](g1148.png)

Now we can add the code that we want to run if the player types in `makestone`.

![on chat](g1152.png)

Search for and click on `getTargetBlock`.

![on chat](g1157.png)

Search for and click on `setType`.

![on chat](g1162.png)

We can't add more right now, so click on `place`.

![on chat](g1166.png)

Now we can choose the block's new type.

![on chat](g1170.png)

Choose `stone`.

![on chat](g1174.png)

Now you can test your code by running your server, opening up minecraft to version 1.16.5, entering your server through multiplayer, looking down at a block, and typing `makestone`. We will review what this code means more in class.
