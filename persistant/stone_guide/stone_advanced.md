# advanced versions of "make stone"

* change the text you need to type to make a block stone
* add a "makeglass" command that makes things into glass
* make a magic wand that converts things into stone
