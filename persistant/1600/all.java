PLAYER:

package space.codekingdoms.alexteacher8.sheep10;

import org.bukkit.Location;
import com.codekingdoms.nozzle.base.BasePlayer;
import java.lang.Float;
import java.lang.Integer;
import java.lang.Math;
import com.codekingdoms.nozzle.utils.Direction;
import org.bukkit.World;
import com.codekingdoms.nozzle.utils.Random;

public class Player extends BasePlayer {


}

GAME:

package space.codekingdoms.alexteacher8.sheep10;

import com.codekingdoms.nozzle.base.BaseGame;
import com.codekingdoms.nozzle.utils.Random;
import org.bukkit.Location;

public class Game extends BaseGame {
	
	public Location randomLocation( Location inputLocation ) {
		
		double newX = inputLocation.getX() + Random.generateInteger(-15, 15);
		double newY = inputLocation.getY();
		double newZ = inputLocation.getZ() + Random.generateInteger(-15, 15);
		return new Location(world, newX, newY, newZ);
	
	}
	
	
}

SHEEP:

package space.codekingdoms.alexteacher8.sheep10;

import com.codekingdoms.nozzle.base.BaseSheep;
import org.bukkit.Location;

public class Sheep extends BaseSheep {
	
	public void onSheared( String playerName, Location inputLocation ) {
		
		Sheep s = new Sheep();
		s.spawn(world, getGame().randomLocation(inputLocation));
	
	}
	
	
}
