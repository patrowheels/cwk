public class Game extends BaseGame {
	public Location randomLoc;
	public void randomLocation(Location inLocation) {
		double newX = inLocation.getX() + 5;
		double newY = inLocation.getY() + 5;
		double newZ = inLocation.getZ() + 5;
		randomLoc = new Location(world, newX, newY, newZ);
	}
