package space.codekingdoms.alexteacher8.methods;

import com.codekingdoms.nozzle.base.BasePlayer;
import org.bukkit.potion.PotionEffectType;
import com.codekingdoms.nozzle.utils.ArmorSet;
import org.bukkit.Material;
import org.bukkit.Location;

public class Player extends BasePlayer {
	
	public void onRunCommand( String command, String[] args ) {
		
		if (command.equals("/clearmobs")) {
			
			getGame().removeAllMobs();
			
		}
		
		else if (command.equals("/invis")) {
			
			applyPotionEffect(PotionEffectType.INVISIBILITY, 600, 1);
			applyPotionEffect(PotionEffectType.HEAL);
			
		}
		
	
	}
	
	public void onJoin() {
		
		addItemToInventory(Material.DIAMOND_SWORD);
		equipFullArmorSet(ArmorSet.DIAMOND);
	
	}
	
	public void onRightClick() {
		
		Enderman e = new Enderman();
		Location playerLoc = getLocation();
		Location spawnLoc = new Location(world, playerLoc.getX() + 10, playerLoc.getY(), playerLoc.getZ() + 10);
		e.spawn(world, spawnLoc);
	
	}
	
	
}
