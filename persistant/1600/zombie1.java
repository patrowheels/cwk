ZOMBIE:

public class Zombie extends BaseZombie {
	
	public void onKilledByPlayer( String playerName ) {
		Zombie z = new Zombie();
		z.spawn(world, getGame().randomLocation(getGame().currentPlayer.getLocation())); // changed
		getGame().score += 1; // new
	}
	
	public void onSpawn() { // new method
		equipItem(Material.LEATHER_HELMET);
		setHealth(1);
		setBaby(false);
	}
}

PLAYER:

public class Player extends BasePlayer {
	public void onRunCommand( String command, String[] args ) {
		if (command.equals("/start")) {
			getGame().start(getLocation());
		}
	}
}

GAME:


public class Game extends BaseGame {
	int score; // new
	Player currentPlayer; // new
	
	public Location randomLoc;
	
	public void start( Player playerParameter ) { // changed
		score = 0; // new
		currentPlayer = playerParameter; // new
		startTimer(30); // new
		Zombie z = new Zombie();
		Location spawnLocation = new randomLocation(currentPlayer.getLocation()); // new
		z.spawn(world, spawnLocation); // changed
		
	}
	
	public Location randomLocation( Location inputLocation ) {
		
		double newX = inputLocation.getX() + Random.generateInteger(-15, 15);
		double newY = inputLocation.getY();
		double newZ = inputLocation.getZ() + Random.generateInteger(-15, 15);
		randomLoc = new Location(world, newX, newY, newZ);
		return randomLoc;
	
	}
	
	public void onTimerExpire() { // new method
		broadcastMessage("Score: " + score);
		cancelTimer();
		removeAllMobs();
	}
	
}
