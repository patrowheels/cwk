question_list = []
answer_list = []
correct_answer_list = []
sprite = codesters.Sprite("puppy_masked", 175, -150) # new line
sprite.set_size(.5) # new line
score = 0 # new line

question = codesters.Text("question", 0, 200)
a = codesters.Text("a", -100, 100)
b = codesters.Text("b", 100, 100)
c = codesters.Text("c", -100, -100)
d = codesters.Text("d", 100, -100)

question_list.append(
    "Who is the main character in the Legend of Zelda games?"
)
answer_list.append(
    [
        "Zelda",
        "Ganon",
        "Navi",
        "Link",
    ]
)

correct_answer_list.append("d")

question_list.append(
    "Which fruit is yellow?"
)
answer_list.append(
    [
        "Apple",
        "Banana",
        "Orange",
        "Strawberry",
    ]
)
correct_answer_list.append("b")

question_list.append(
    "Which minecraft ore looks like red specs?"
)
answer_list.append(
    [
        "Redstone",
        "Gold",
        "Emerald",
        "Diamond",
    ]
)
correct_answer_list.append("a")

# A lot changed at the bottom

for question_session in [0, 1, 2]:
    question.set_text(question_list[question_session])
    a.set_text("A: " + answer_list[question_session][0])
    b.set_text("B: " + answer_list[question_session][1])
    c.set_text("C: " + answer_list[question_session][2])
    d.set_text("D: " + answer_list[question_session][3])
    
    user_answer = sprite.ask("What's the answer?")
    user_answer = user_answer.lower()
    if user_answer == correct_answer_list[question_session]:
        score = score + 1
        
question.set_text("")
a.set_text("")
b.set_text("")
c.set_text("")
d.set_text("")

sprite.go_to(0, 0)

letter_string = "s"
if score == 1:
    letter_string = ""
sprite.say(f"You got {score} answer{letter_string} right!")
