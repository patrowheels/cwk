class Pet:# blueprint part
    def __init__(self, _name, _age, _type):# instructions to create yourself
    # customize name and age
        self.name = _name
        self.age = _age # save name and age
        self.type = _type
    
    def print_info(self):
        print(
            f"{self.name} is a {self.age} year old {self.type}"
        )

    def do_a_trick(self):
        print(
            f"You see the {self.type} do a backflip!"    
        )

class Dog(Pet):
    def __init__(self, _name, _age):
        super().__init__(_name, _age, _type="Dog")
        
    def speak(self):
        print("bark bark bark!")
        

class Robot(Pet):
    def __init__(self, _name, _age):
        super().__init__(_name, _age, _type="Robot")
    
    def speak(self):
        print("beep boop beep!")
        
    def do_a_trick(self):
        print(
            f"You see the {self.type} scan a card and run a program"   
        )

        
# make a pet from blueprint
my_pet = Pet(_name="Kung Fu Panda", _age=5, _type="Panda")
my_pet.print_info() # Kung Fu Panda is a 5 year old Panda
my_pet.do_a_trick() # You see the Panda do a backflip!

print()

blaze = Dog(_name="Blaze", _age=1)
blaze.print_info() # Blaze is a 1 year old Dog
blaze.speak() # bark bark bark!
blaze.do_a_trick() # You see the Dog do a backflip!


print()

altbot = Robot(_name="Altbot", _age=3)
altbot.print_info() # Altbot is a 3 year old Robot
altbot.speak() # beep boop beep!
altbot.do_a_trick() # You see the Robot scan a card and run a program


"""
In this example, the "Dog" class and the "Robot" class
both inherit from the "Pet" class.

Try to make a new class called "Wolf" or "Falcon" or
another cool pet that inherits from the "Pet" class.

You can look to "Dog" and "Robot" for hints on the
syntax.
"""

