# ==================== SETUP ENEMY ====================

class Enemy: # Enemey blueprint
    def __init__(self, _health, _image_name):
        self.sprite = codesters.Sprite(_image_name)
        self.sprite.set_size(.2)
        self.health = _health
        self.sprite.say(self.health)
        self.sprite.parent = self

    def announce_health(self):
        self.sprite.say(self.health)

# ==================== SETUP PLAYER ====================

class Player:
    def __init__(self, _health):
        self.sprite = codesters.Sprite(
            "chick1", # sprite
            0, # x
            -150, # y
        )
        self.sprite.set_size(.4)

    # Not necessary
    def grow_and_shrink(self):
        for i in range(5):
            self.sprite.set_size(1/.8)
            stage.wait(0.5)
        stage.wait(2)
        for i in range(5):
            self.sprite.set_size(.8)
            stage.wait(0.5)

        
            
# ==================== SETUP PLAYER MOVEMENT ====================

hero = Player(10)
# hero.grow_and_shrink()

# ==================== RUN THE GAME ===================

one_enemy = Enemy(_health = 3, _image_name="snake2") # "Follow the blueprint and make an EnemyCobra

def click(sprite):
    sprite.parent.health -= 1
    sprite.parent.announce_health()
    
one_enemy.sprite.event_click(click)


"""
while one_enemy.health > 0:
    stage.wait(1)
    one_enemy.health -= 1
    one_enemy.announce_health()
"""


"""
GOALS:
  * A bunch of different types of enemies
    * Different levels of health
    * Different movement patterns
"""




