stage.disable_floor()
stage.disable_ceiling()   
stage.set_background("castle3")


# ==================== SETUP ENEMY ====================

class Enemy: # Enemey blueprint
    def __init__(self,
        _health=3,
        _image_name="snake2",
        _x=0,
        _size=.2,
        _y=200,
        _y_speed=-4): # changed
        self.sprite = codesters.Sprite(_image_name, 1000, 1000) # changed
        self.sprite.set_size(_size)
        self.sprite.go_to(_x, _y) # added
        self.health = _health
        self.sprite.say(self.health)
        self.sprite.parent = self
        self.sprite.set_y_speed(_y_speed) # added

    def announce_health(self):
        self.sprite.say(self.health)

# ==================== SETUP PLAYER ====================

class Player:
    def __init__(self, _health):
        self.sprite = codesters.Sprite(
            "chick1", # sprite
            0, # x
            -225, # y
        )
        self.sprite.set_size(.4)

    # Not necessary
    def grow_and_shrink(self):
        for i in range(5):
            self.sprite.set_size(1/.8)
            stage.wait(0.5)
        stage.wait(2)
        for i in range(5):
            self.sprite.set_size(.8)
            stage.wait(0.5)


            
# ==================== SETUP PLAYER MOVEMENT ====================

hero = Player(10)

def right_key_ron(sprite_hermione):
    sprite_hermione.move_right(20)

hero.sprite.event_key("right", right_key_ron)


def left_key_sirius(sprite_riddle):
    sprite_riddle.move_left(20)

hero.sprite.event_key("left", left_key_sirius)

def collision_roblox(red, enemy):
    enemy_name = enemy.get_image_name() 
    if enemy_name in ["evilwitch", "snake2"]:
        stage.remove_sprite(red)
        if hasattr(enemy, "parent") and enemy.parent:
            enemy.parent.health -= 1
            enemy.parent.announce_health()
            if enemy.parent.health <= 0:
                stage.remove_sprite(enemy)

def fire_obiwan(sprite_mando):
    red = codesters.Sprite(
        "red_laser_19d", # custom sprite name
        1000, # x
        1000, # y
    )
    red.event_collision(collision_roblox)
    # set up laser here
    red.set_size(.3)
    
    red.go_to(
        sprite_mando.get_x(), # x
        sprite_mando.get_y(), # y
    )
    
    red.set_y_speed(5) # make it fire up
    
    # new line at top: stage.disable_all_walls()

hero.sprite.event_key("space", fire_obiwan)

# ==================== RUN THE GAME ===================

def spawn_enemy(enemy_type):
    if enemy_type == "cobra":
        """
        one_enemy = Enemy(
            _health = 3, 
            _image_name = "snake2", 
            _x = random.randint(-150, 150), 
            _size = .2,
        ) # "Follow the blueprint and make an EnemyCobra
        """
        one_enemy = Enemy(
            _x=random.randint(-150, 150)    
        )
    elif enemy_type == "witch":
        one_enemy = Enemy(
            _health = 30,
            _image_name = "evilwitch",
            _x = random.randint(-25, 25), 
            _size = .7,
            _y_speed = -1,
        )

counter = 0
while True:
    stage.wait(.1)
    if counter % 5 == 0:
        spawn_enemy(enemy_type="cobra")
    if counter % 22 == 0:
        spawn_enemy(enemy_type="witch")
    counter = counter + 1


