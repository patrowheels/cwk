# ========== SETUP TURNS ==========

turn_options = ["x", "o"]
turn_index = 0

def switch_turn():
    global turn_index
    turn_index = (turn_index + 1) % 2

grid = []
for i in range(3):
    row = []
    for j in range(3):
        row.append("e")
    grid.append(row)
    
print(grid)

squares = []
for y_coord in [100, 0, -100]:
    for x_coord in [-100, 0, 100]:
        one_square = codesters.Rectangle(
            x_coord,
            y_coord,
            96,
            96,
            "white"
        )
        one_square.set_outline_color("black")
        one_square.set_line_thickness(4)
        squares.append(one_square)
        
for i in range(9):
    squares[i].id = i
    # squares[i].say(i)

def click(sprite):
    global turn_index
    player_letter = turn_options[turn_index] # new
    row = sprite.id // 3
    col = sprite.id % 3
    if grid[row][col] == "e": # new
        grid[row][col] = player_letter # changed
        letter = codesters.Text(
            player_letter, 
            sprite.get_x(),
            sprite.get_y(),
        )
        letter.set_size(4)
        switch_turn() # changed
        print(grid) # changed

for one_square in squares:    
    one_square.event_click(click)
