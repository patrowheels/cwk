# ========== SETUP TURNS ==========

turn_options = ["x", "o"]
turn_index = 0

def switch_turn():
    global turn_index
    turn_index = (turn_index + 1) % 2

grid = []
for i in range(3):
    row = []
    for j in range(3):
        row.append("e")
    grid.append(row)
    
print(grid)

squares = []
for y_coord in [100, 0, -100]:
    for x_coord in [-100, 0, 100]:
        one_square = codesters.Rectangle(
            x_coord,
            y_coord,
            96,
            96,
            "white"
        )
        one_square.set_outline_color("black")
        one_square.set_line_thickness(4)
        squares.append(one_square)
        
for i in range(9):
    squares[i].id = i
    # squares[i].say(i)

def click(sprite):
    global turn_index
    player_letter = turn_options[turn_index] # new
    row = sprite.id // 3
    col = sprite.id % 3
    if grid[row][col] == "e": # new
        grid[row][col] = player_letter # changed
        letter = codesters.Text(
            player_letter, 
            sprite.get_x(),
            sprite.get_y(),
        )
        letter.set_size(4)
        switch_turn() # changed
        print(grid) # changed
        if check_win( # temporary
            player='x',
            position_x=0,
            position_y=0,
            direction_x=0,
            direction_y=1,
        ):
            print("x wins!")

for one_square in squares:    
    one_square.event_click(click)

def check_win(
    player, # this can be 'x' or 'o'
    position_x, # the x position we're looking at
    position_y, # the y position we're looking at
    direction_x, # the direction we're changing x
    direction_y, # the direction we're changing y
    win_matching = 3, # we need 3-in-a-row to win
    counter = 0,
    ):
    counter = counter + 1
    if grid[position_y][position_x] != player:
        return False
    else:
        if counter == win_matching:
            return True
        else:
            return check_win(
                player,
                position_x + direction_x,
                position_y + direction_y,
                direction_x,
                direction_y,
                win_matching,
                counter,
                )




