import csv
# CODE THAT PULLS FROM CSV
with open("Product.csv") as f:
    full_product_list = list(
            csv.DictReader(f)
    )

while True:
  # CODE THAT ASKS REPEATEDLY (REPL)
  user_choice = input("Give name: ")

  # EXIT IF THEY TYPE 'q'
  if user_choice == 'q':
    break
  
  # LOOP OVER ROWS (LOOP OVER DICTIONARIES IN LIST)
  for one_row in full_product_list:
    # IF WE FIND ONE WITH THE MATCHING NAME
    if one_row['Name'] == user_choice:
      # PRINT INFO
      print(f"{one_row['Name']} is in category {one_row['Category (FK)']}")

