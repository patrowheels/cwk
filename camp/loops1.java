package space.codekingdoms.alexteacher8.loopexamples;

import com.codekingdoms.nozzle.base.BasePlayer;

public class Player extends BasePlayer {
	public void onRunCommand(String command, String[] args) {
		if (command.equals("/loop1")) {
			String[] foods = {"burger", "macncheese", "ice cream", "curry chicken", "pizza",
			"takis",};
			for ( String oneFood : foods ) {
				chat(oneFood);
			}
		}
		else if (command.equals("/loop2")) {
			String[] letters = {"a", "b", "c"};
			int length = letters.length;
			for (int i=0; i<length; i++) {
				chat(letters[i]);
			}
		}
	}
}
