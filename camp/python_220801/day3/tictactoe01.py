grid = []
for i in range(3):
    row = []
    for j in range(3):
        row.append("empty")
    grid.append(row)
    
print(grid)

squares = []
for y_coord in [100, 0, -100]:
    for x_coord in [-100, 0, 100]:
        one_square = codesters.Rectangle(
            x_coord,
            y_coord,
            96,
            96,
            "white",
        )
        one_square.set_outline_color("black")
        one_square.set_line_thickness(4)
        squares.append(one_square)
        
        
for i in range(9):
    squares[i].id = i
    squares[i].say(i)

def click(sprite):
    grid[sprite.id//3][sprite.id%3] = "x"
    print(grid)
    
for one_square in squares:
    one_square.event_click(click)
