class MapCell:
    def __init__(
        self,
        n_wall,
        s_wall,
        e_wall,
        w_wall,
        NorthCell=None,
        SouthCell=None,
        EastCell=None,
        WestCell=None,
    ):
        self.NCell = NorthCell
        self.SCell = SouthCell
        self.ECell = EastCell
        self.WCell = WestCell
        self.n_wall = n_wall
        self.s_wall = s_wall
        self.e_wall = e_wall
        self.w_wall = w_wall
    
    def look_north(self):
        print("You see", self.n_wall)
        
    def look_south(self):
        print("You see", self.s_wall)
        
    def look_east(self):
        print("You see", self.e_wall)
        
    def look_west(self):
        print("You see", self.w_wall)

starting_cell = MapCell(
    n_wall = "a locked door that needs a key.",
    s_wall = "a chest sitting on an ornate rug.",
    e_wall = "a dusty window with light barely shining through.",
    w_wall = "a bookcase filled with sophisticated books.",
)

user_options = ["help", "look", "open", "quit"]

game_running = True
while game_running == True:
    print("commands:", user_options)
    user_choice = input("What do you choose?")
    if user_choice.split()[0] in user_options:
        user_choice_first = user_choice.split()[0]
        if user_choice_first == "quit":
            confirm = input("Really quit?")
            if confirm in ['y', 'Y', 'yes', 'Yes', 'YES']:
                game_running = False
    else:
        print("Invalid command, try again.")
