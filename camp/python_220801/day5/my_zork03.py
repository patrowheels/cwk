# ========== SETUP INVENTORY ==========

inventory = []

# ========== SETUP ITEM CLASS ==========

class Item:
    """This class can be a chest, a button, a rug, a sword, a key"""
    def __init__(
    
        self, 
        description, # a description of the item 
        state, # the state of the item
        contains=None, # does the item contain stuff?
        open_verb=None, # what happens when you open?
        move_verb=None, # what happens when you move?
    
    ):
        
        # saving everything to self
        self.description = description
        self.state = state
        self.open_verb = open_verb
        self.move_verb = move_verb
        
        if contains is None:
            self.contains = []
        else:
            self.contains = contains
            
            
# ========== SETUP MAP CELL ==========

class MapCell:
    def __init__(
        self,
        n_wall,
        s_wall,
        e_wall,
        w_wall,
        NorthCell=None, # won't use
        SouthCell=None, # won't use
        EastCell=None, # won't use
        WestCell=None, # won't use
    ):
        self.NCell = NorthCell # won't use
        self.SCell = SouthCell # won't use
        self.ECell = EastCell # won't use
        self.WCell = WestCell # won't use
        self.n_wall = n_wall
        self.s_wall = s_wall
        self.e_wall = e_wall
        self.w_wall = w_wall
    
    def look_north(self):
        print("You see", self.n_wall)
        
    def look_south(self):
        print("You see", self.s_wall)
        
    def look_east(self):
        print("You see", self.e_wall)
        
    def look_west(self):
        print("You see", self.w_wall)

# ========== SETUP STARTING CELL ==========

starting_cell = MapCell(
    n_wall = "a locked door that needs a key.",
    s_wall = "a chest sitting on an ornate rug.",
    e_wall = "a dusty window with light barely shining through.",
    w_wall = "a bookcase filled with sophisticated books.",
)

# ========== SETUP ITEMS ==========

def open_journal(self):
    print("You open up your journal to see an old drawing")
    
journal = Item(
    description = "a worn book that I like to draw in",
    state = "worn",
    open_verb = open_journal,
)

inventory.append(journal)

door_key = Item(
    description = "a rusty key",
    state = "fine",
)

sword = Item(
    description = "a dusty steel sword",
    state = "okay",
)

chest_key = Item(
    description = "a small, odd key",
    state = "odd",
)

rug = Item(
    description = "a frayed rug, once quite valuable",
    state = "frayed",
    contains = [chest_key],
)

def open_chest(self):
    global inventory
    if chest_key in inventory: # the user has the chest key
        print("You used the key to open the chest!")
        # the user gains all the items
        for item in self.contains:
            inventory.append(item)
    else: # the user doesn't have the chest key
        print("It's locked!")

chest = Item(
    description = "a simple chest with an old lock",
    state = "battered",
    contains = [sword, door_key],
    open_verb = open_chest,
)

door = Item(
    description = "a door blocking your path",
    state = "locked",
)

# ========== SETUP GAME LOOP ==========

user_options = ["help", "look", "open", "quit"]

game_running = True
while game_running == True:
    
    print("commands:", user_options)
    user_choice = input("What do you choose?")
    if user_choice.split()[0] in user_options:
        user_choice_first = user_choice.split()[0]
        if user_choice_first == "quit":
            confirm = input("Really quit?")
            if confirm in ['y', 'Y', 'yes', 'Yes', 'YES']:
                game_running = False
    else:
        print("Invalid command, try again.")
