class Face:
    def __init__(self):
        self.head = codesters.Circle(
            0, # x
            100, # y
            150, # diameter
            "gray", # color
        )
        self.head.set_outline_color("black")
        self.head.set_line_thickness(2)
        self.left_eye = codesters.Circle(
            -20, # x
            120, # y
            30, # diameter
            "white", # color
        )
        self.left_eye.set_outline_color("black")
        self.left_pupil = codesters.Circle(
            -20, # x
            120, # y
            10, # diameter,
            "black", # color
        )
        
    def look(self): # new
        x_pos, y_pos = stage.mouse_x(), stage.mouse_y()
        hyp = math.sqrt(
            x_pos ** 2 + y_pos ** 2
        )
        x_pos = x_pos/hyp
        y_pos = y_pos/hyp
        self.left_pupil.go_to(x_pos, y_pos)
        
my_face = Face()
stage.event_mouse_move(my_face.look) # new

# hint code

mouth_middle = codesters.Rectangle(
    -100,
    0,
    50,
    20,
    "blue",
)

mouth_left = codesters.Rectangle(
    -140,
    20,
    50,
    20,
    "blue",
)
mouth_left.set_rotation(-45)


def change_mouth_to_circle():
    for mouth_piece in [mouth_middle, mouth_left]:
        mouth_piece.hide()
        
    mouth_circle = codesters.Circle(
        -100,
        0,
        70,
        "blue",
    )


stage.wait(2)
change_mouth_to_circle()


