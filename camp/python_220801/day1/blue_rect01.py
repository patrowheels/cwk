# Creating a rectangle

my_blue_rect = codesters.Rectangle(
    0, # x
    100, # y
    100, # width
    50, # height
    "blue", # color
)

# Giving the rectangle something to remember
my_blue_rect.name = "Blu"


# Listing everything that the rectangle remembers (mostly verbs)
"""
print(
    "\n".join(dir(my_blue_rect))
)
"""

# Printing out the name that it remembers
print(
    my_blue_rect.name
)
