class Face:
    def __init__(self):
        self.head = codesters.Circle(
            0, # x
            100, # y
            150, # diameter
            "gray", # color
        )
        self.head.set_outline_color("black")
        self.head.set_line_thickness(2)
        
my_face = Face()
