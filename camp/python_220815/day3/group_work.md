# Group Work

* Make the colors alternate with the letters, so that every `X` is blue and every `O` is red

## Stretch Goal

* Randomize it at the beginning, so every `X` has a set random color