grid = []

for i in range(3): # [0, 1, 2]
    row = []
    for j in range(3): # [0, 1, 2]
        row.append("empty")
    grid.append(row)

squares = []
for y_coord in [100, 0, -100]:
    for x_coord in [-100, 0, 100]:
        one_square = codesters.Rectangle(
            x_coord,
            y_coord,
            96,
            96,
            "white",
        )
        one_square.set_outline_color("black")
        one_square.set_line_thickness(4)
        squares.append(one_square)


"""
row1 = []
for j in range(3): # [0, 1, 2]
    row1.append("empty")
grid.append(row1)

row2 = []
for j in range(3): # [0, 1, 2]
    row2.append("empty")
grid.append(row2)

row3 = []
for j in range(3): # [0, 1, 2]
    row3.append("empty")
grid.append(row3)

print(row1, row2, row3, sep="\n")
"""
#print(*grid, sep="\n") 
