from collections import namedtuple
import random

Enemy = namedtuple("Enemy", "x y".split())

enemies = [
  Enemy(
    x = random.randint(-250, 250),
    y = random.randint(-200, 200),
  ) for i in range(20)
]

# print(enemies)


Hero = namedtuple("Hero", "x y".split())

hero = Hero(
  x = random.randint(-50, 50),
  y=-200,
)

def prioritize_enemy(_enemy):
  points = 0
  if _enemy.y < -100:
    points += abs(_enemy.y) * 1000
  points -= abs(hero.x - _enemy.x)
  # print(_enemy.x, _enemy.y, points)
  return points

enemies.sort(
  key = prioritize_enemy, reverse = True,
)

print(f"{hero=}")
print(enemies)
high_priority_enemy = enemies[0]
print(f"{high_priority_enemy=}")
