class Face:
    def __init__(self,
        # DEFAULTS
        eye_height = 120,
        eye_distance_from_middle = 50,
        head_size = 300,
        head_color = "gray",
        eye_size = 50,
        pupil_size = 10,
        ):
        self.head = codesters.Circle(
            0, # x
            100, # y
            head_size, # diameter
            head_color, # color
        )
        
        self.left_eye = codesters.Circle(
            -1 * eye_distance_from_middle, # x
            eye_height, # y
            eye_size, # diameter
            "white", # color
        )
        self.left_eye.set_outline_color("black")
        
        self.left_pupil = codesters.Circle(
            -1 * eye_distance_from_middle, # x
            eye_height, # y
            pupil_size, # diameter,
            "black", # color
        )
        
        self.left_eyelid = codesters.Circle(
            -1 * eye_distance_from_middle, # x
            eye_height, # y
            eye_size, # diameter
            "gray", # color
        )
        self.left_eyelid.hide()
        self.left_eyelid.move_to_front()
        
    def wink(self):
        self.left_eyelid.show()
        stage.wait(0.1)
        self.left_eyelid.hide()
        
my_face = Face()

while True:
    stage.wait(
        random.randint(2, 5)
    )
    my_face.wink()


