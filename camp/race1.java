GAME:

package space.codekingdoms.alexteacher8.race;

import com.codekingdoms.nozzle.base.BaseGame;
import org.bukkit.Location;

public class Game extends BaseGame {
	
	public Location startLocation;
	
	public Player currPlayer;
	
	public void onTimerExpire() {
		Location endLocation = currPlayer.getLocation();
		double dist = endLocation.distance(startLocation);
		currPlayer.sendMessage("Distance: " + dist);
	}
	
	public void start( Player player ) {
		currPlayer = player;
		startLocation = currPlayer.getLocation();
		startTimer(30);
	}	
}


PLAYER:

package space.codekingdoms.alexteacher8.race;

import com.codekingdoms.nozzle.base.BasePlayer;

public class Player extends BasePlayer {
	public void onRunCommand(String command, String[] args) {
		if (command.equals("/start")) {
			getGame().start(this);

		}
	}
	
}
