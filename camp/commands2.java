package space.codekingdoms.alexteacher8.commands;

import com.codekingdoms.nozzle.base.BasePlayer;

public class Player extends BasePlayer {
	public void onRunCommand(String command, String[] args) {
		if (command.equals("/a")) {
			world.strikeLightning(getTargetBlock().getLocation());
		}
		/*
		if (true or false thing) {
			runCommand();
		}
		*/
	}
}
/* more commands:
applyPotionEffect(PotionEffectType.INVISIBILITY, 600, 1);
setWalkSpeed(0.5f);
setFoodLevel(20);
setHealth(20);
addItemToInventory(new ItemStack(Material.DIAMOND_SWORD));
*/

/*
"/a".equals("/a")
check if command equals "/a"
if (x > y) {
  System.out.println("x is greater than y");
}
*/
