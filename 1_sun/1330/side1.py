def make_id():
    id = ""
    for i in range(10):
        id = id + random.choice("0123456789abcde")
    return id

# ========== SETUP BLOCKS ==========

blocks = []

def create_blocks():
    block = codesters.Rectangle(
        260, -175, 50, 100, "darkred", 
    )
    blocks.append(block)
    block.set_x_speed(-3)
    block.set_gravity_off()
    block.id = make_id()

# ========== SETUP STAGE ==========
stage.set_gravity(10)
stage.disable_all_walls()

# ========== SETUP FLOOR ==========

floor = codesters.Rectangle(
    0, # x pos
    -235, # y pos
    600, # width
    50, # height
    "darkred",
)
floor.set_gravity_off()
floor.id = make_id()

# ========== SETUP HERO ==========


hero = codesters.Sprite("alien1",-150, -170)
hero.set_size(.5)
hero.set_y(
    floor.get_y() + (floor.get_height()/2) + (hero.get_height()/2)
)
hero.health = 5
hero.grounded = True
hero.on = None

# ========== SETUP TEXT ==========

health_text = codesters.Text(f"health: {hero.health}", -175, 200)

def refresh_health_text():
    health_text.set_text(f"health: {hero.health}")

grounded_text = codesters.Text(
    f"grounded: {hero.grounded}", -175, 180    
)

def refresh_grounded_text():
    grounded_text.set_text(f"grounded: {hero.grounded}")
    
on_text = codesters.Text(f"on: {hero.on}", -175, 160)

def refresh_on_text():
    on_text.set_text(f"on: {hero.on}")

# ========== SETUP MOTION ==========

def space_bar():
    if hero.grounded:
        hero.grounded = False
        refresh_grounded_text()
        hero.jump(15)
        hero.set_gravity_on()
    
stage.event_key("space", space_bar)


"""
def left_key(sprite):
    sprite.move_left(20)

sprite.event_key("left", left_key)

def right_key(sprite):
    sprite.move_right(20)

sprite.event_key("right", right_key)
"""
# ========== SETUP COLLISION ==========

def collision(sprite, hit_sprite):
    my_var = hit_sprite.get_color() 
    if my_var == "darkred":
        sprite.set_y_speed(0)
        sprite.set_gravity_off()
        hero.set_y(
            hit_sprite.get_y() + \
            (hit_sprite.get_height()/2) + \
            (hero.get_height()/2)
        )
        sprite.grounded = True
        refresh_grounded_text()
        sprite.on = hit_sprite.id
        refresh_on_text()

hero.event_collision(collision)


counter = 0
while True:
    wait_amount = 10 * random.randint(1, 8)
    stage.wait(.1)
    if counter % wait_amount == 0:
        create_blocks()
    counter = counter + 1
    for block in blocks:
        if block.id == hero.on:
            block.check_on(): # not written yet

"""
# TEMPORARY CODE
while True: # forever loop
    stage.wait(1)
    hero.say("Ouch!", 0.3)
    hero.health = hero.health - 1
    refresh_health_text()
"""

