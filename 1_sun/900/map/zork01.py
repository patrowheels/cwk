# STATES

class Room:
    def __init__(
        self,
        main_description,
        f_description,
        b_description,
        l_description,
        r_description,
        ):
        self.main_description = main_description
        self.f_description = f_description
        self.b_description = b_description
        self.l_description = l_description
        self.r_description = r_description
        

STATE_LIST = [
    Room(
        main_description = "",
        f_description = "",
        b_description = "",
        l_description = "",
        r_description = "",
        ),
]

STATE = 0

GAME_RUNNING = True

# GAME LOOP

while GAME_RUNNING:
    print(STATE_LIST[STATE])
    print("What do you want to do?")
    user_choice = input(": ")
    if user_choice.lower() in ('h', 'help'):
        print('look (f)orward, (b)ack, (l)eft, (r)ight')
        print('(q) to quit')
    if user_choice.lower() in ('q', 'quit'):
        GAME_RUNNING = False

