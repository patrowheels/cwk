//var, let, and const.

//OBJECTS
// red square
var player;
var playerSize = 10;

// pillars / obstacles
var obstacles;

// floor
var canvas = document.getElementById("myCanvas-friedchicken");
var ctx = canvas.getContext("2d");
var floor = 20;

// score
var scoreText = document.getElementById("score");
var scoreSpan = document.getElementById("score-number");
var score;

// array of obstacles
var obsArray = [];
// prompt text

//NON-OBJECT GLOBALS
var gameOver = false; // True or false
var scroll; // movement to left
var updateCounter = 0;


//FUNCTIONS
function start() {
	score = 0;
	player = {
		x_pos: canvas.width/4,
		y_pos: floor + playerSize,
		y_vel: 0,
		gravityOn: false,
	};
	drawBackground();
	drawGround();
	drawPlayer();
}

function drawBackground() {
	ctx.beginPath();
	ctx.rect(0, 0, canvas.width, canvas.height);
	ctx.fillStyle = "skyblue";
	ctx.fill();
}

function drawGround() {
	ctx.beginPath();
	ctx.rect(0, canvas.height, canvas.width, floor * -1);
	ctx.fillStyle = "black";
	ctx.fill();
}

function drawPlayer() {

	//console.log(player.x_pos);
	//console.log(player.y_pos);
	ctx.beginPath();
	ctx.rect(player.x_pos, canvas.height - player.y_pos, playerSize, playerSize);
	// ctx.rect(player.x_pos, player.y_pos, playerSize, playerSize);
	ctx.fillStyle = "red";
	ctx.fill();
}

function updateState() {
	/*
	player = {
		x_pos: canvas.width/4,
		y_pos: floor + playerSize,
		y_vel: 0,
		gravityOn: false,
	}
	*/	
	player.y_pos += player.y_vel;
	if (player.gravityOn) {
		player.y_vel -= .5;
	}
	/*
	 * floor = 20;
	 * canvas.height = 200;
	 * player.x_pos;
	 * player.y_pos;
	 * player.y_vel;
	 * player.gravityOn;
	if the y position is = floor {
		gravity = false;
	}
	 */
	if (player.y_pos <= floor + playerSize) {
		player.gravityOn = false;
		player.y_vel = 0;
		player.y_pos = floor + playerSize;
	}

}


function keyDownHandler(e) {
	if (e.keyCode === 65) {
		if (gameOver) {
			start();
			gameOver = false; // start the game
		} else if (!player.gravityOn) {
			player.y_vel = 10;
			player.gravityOn = true;
		}
	}
}

document.addEventListener("keydown", keyDownHandler, false);

function spawnObs() {
	var height = 50;
	newObs = {
		x_pos: canvas.width,
		y_pos: canvas.height - height - floor,
		width: 20,
		height: height,
		x_speed: 10,
	};
	obsArray.push(newObs);
	//console.log(obsArray);
}

function drawObstacles() {
	for (let i=0; i<obsArray.length; i++) {
		obsArray[i].x_pos -= obsArray[i].x_speed;
		ctx.beginPath();
		ctx.rect(
			obsArray[i].x_pos,
			obsArray[i].y_pos,
			obsArray[i].width,
			obsArray[i].height,
		);
		ctx.fillStyle = "blue";
		ctx.fill();
	}
}

function checkIn(inX, inY, leftX, rightX, topY, bottomY) {
	return (inX > leftX &&
		inX < rightX &&
		inY > topY &&
		inY < bottomY)
}

function checkDistance(player, obs) {
	let x_difference = player.x_pos - obs.x_pos;
	let y_difference = player.y_pos - obs.y_pos;
	return Math.sqrt(
		x_difference**2 + y_difference**2
	);
}

function checkCollision() {
	let x_pos = player.x_pos;
	let y_pos = canvas.height - player.y_pos;
	let playerCorners = [
		[x_pos, y_pos],
		[x_pos + playerSize, y_pos],
		[x_pos, y_pos + playerSize],
		[x_pos + playerSize, y_pos + playerSize],
	];
	let collision = false;
	for (let j=0; j<obsArray.length; j++) {
		if (checkDistance(player, obsArray[j]) < playerSize * 2) {
			for (let i=0; i<playerCorners.length; i++) {
				if (checkIn(
					playerCorners[i][0],
					playerCorners[i][1],
					obsArray[j].x_pos,
					obsArray[j].x_pos + obsArray[j].width,
					obsArray[j].y_pos,
					obsArray[j].y_pos + obsArray[j].height
				)) {
					collision = true;
				}
			}
		}
	}
	//console.log(y_pos);
}

function frameUpdate() {
	if (!gameOver) {
		updateState();
		drawBackground();
		drawGround();
		drawPlayer();
		updateCounter++;
		if ((updateCounter % 50) == 0) {
			spawnObs();
		}
		drawObstacles();
		let collision = checkCollision();
		//requestAnimationFrame(frameUpdate);
		setTimeout(frameUpdate, 50);
	}
}

start();
requestAnimationFrame(frameUpdate);




/*
 * setInterval example
function incrementMyCounter() {
	myCounter++;
	if (myCounter%100 == 0) {
		console.log(myCounter);
	}
}

setInterval(incrementMyCounter, 10);
*/
/*
let myCounter = 0;

function incrementMyCounter() {
	myCounter++;
	if (myCounter%100 == 0) {
		console.log(myCounter);
	}
	requestAnimationFrame(incrementMyCounter);
}

requestAnimationFrame(incrementMyCounter);
*/


/* TODO
 * Get rid of used obstacles on the left side
 *
 */
