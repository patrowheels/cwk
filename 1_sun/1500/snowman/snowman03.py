# =============== MAKE LIST OF WORDS ===============

words = """
television
roblox
tiger
Pneumonoultramicroscopicsilicovolcanoconiosis
desktop
""".strip().split('\n')
print(words)

# ignore .strip()
# .split is going to create a list
# every line in the string will be an item in the list

# =============== CHOOSE A WORD ===============

answer = random.choice(words) # desktop
# answer = "desktop"

# ========== MAKE UNDERSCORE REPRESENTATION ==========

repr = ["_"] * len(answer) # ["_", "_", "_", "_", "_", "_", "_"]

repr_string = " ".join(repr) # "_ _ _ _ _ _ _"

# ========== CREATE CODESTERS TEXT FOR REP ==========

lines = codesters.Text(
    repr_string, # string
    0, # x
    100, # y
)

while "_" in repr:

    guess = stage.ask("Guess a letter:")
    
    # for index, letter in enumerate(answer):
    for index, letter in enumerate(answer):
        if letter == guess:
            repr[index] = guess
            repr_string = " ".join(repr)
            lines.set_text(repr_string)




