# =============== MAKE LIST OF WORDS ===============

words = """
television
roblox
tiger
Pneumonoultramicroscopicsilicovolcanoconiosis
""".strip().split('\n')
# ignore .strip()
# .split is going to create a list
# every line in the string will be an item in the list

# =============== CHOOSE A WORD ===============

answer = random.choice(words) # tiger

# ========== MAKE UNDERSCORE REPRESENTATION ==========

repr = ["_"] * len(answer) # ["_", "_", "_", "_", "_"]

repr_string = " ".join(repr) # "_ _ _ _ _"

# ========== CREATE CODESTERS TEXT FOR REP ==========

lines = codesters.Text(
    repr_string, # string
    0, # x
    100, # y
)
