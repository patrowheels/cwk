# GET THE FIRST NUMBER
num1_str = input("first number: ")
num1_int = int(num1_str)

# GET THE SECOND NUMBER
num2_str = input("second number: ")
num2_int = int(num2_str)

print(f"{num1_int} divided by {num2_int} is {num1_int//num2_int}")
print(f"with a remainder of {num1_int % num2_int}")
