stage.set_gravity(10)

hero = codesters.Sprite("alien1_masked", -200, -150)
hero.set_size(.5)
hero.grounded = False
hero.jumps = 2
hero.say(hero.jumps)
hero.jump_lock = False

grounded_text = codesters.Text(
    f"Grounded: {hero.grounded}",
    -160, # x
    200 # y
)

def update_grounded_text():
    grounded_text.set_text(f"Grounded: {hero.grounded}")
    
floor = codesters.Rectangle(
    0, # x
    -225, # y
    500, # width
    50, # height
    "red", # color
)

def collision_minecraft(_hero, _rect):
    rect_color = _rect.get_color() 
    if rect_color == "red": # colliding with floor
        hero.jumps = 2 # new
        hero.say(hero.jumps) # new
        _hero.set_y(
            _rect.get_y() + \
            _rect.get_height() // 2 + \
            _hero.get_height() // 2 + 1
        )
        _hero.set_gravity_off()
        _hero.set_y_speed(0)
        _hero.grounded = True
        update_grounded_text()

hero.event_collision(collision_minecraft)

def space_bar_nether():
    if hero.jumps > 0 and hero.jump_lock == False: # changed
        hero.jump_lock = True
        hero.jumps -= 1
        hero.say(hero.jumps)
        hero.jump(15)
        hero.set_gravity_on()
        hero.grounded = False
        update_grounded_text()
    
stage.event_key("space", space_bar_nether)

def release_key(_hero):
    _hero.jump_lock = False

hero.event_key_release("space", release_key)

def right_key():
    if hero.get_x() < -100:
        hero.move_right(20)

stage.event_key("right", right_key)

def left_key():
    if hero.get_x() > -225:
        hero.move_left(20)
        
stage.event_key("left", left_key)

def down_key():
    if hero.grounded == False:
        hero.set_y_speed(-15)

stage.event_key("down", down_key)
