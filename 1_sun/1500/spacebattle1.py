# ===== SETUP STAGE =====
stage.set_background("space4")

# ===== SETUP SPRITES =====

ufo = codesters.Sprite("ufo", 500, 500)
ufo.set_size(.3)
ufo.go_to(
    200, # x position 
    0, # y position
)

shuttle = codesters.Sprite("spaceshuttle", 500, 500)
shuttle.set_size(.3)
shuttle.go_to(
    -200, # x position 
    0, # y position
)

# ===== SETUP MOVEMENT =====

def left_key_jurassic(sprite):
    sprite.move_left(20)
    sprite.set_rotation(0)

ufo.event_key("left", left_key_jurassic)

def right_key_spiderman(sprite):
    sprite.move_right(20)
    sprite.set_rotation(-180)

ufo.event_key("right", right_key_spiderman)

# ===== SETUP OBSTACLES =====

boundary = 175

# for i in [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49]:
for i in range(50):
    obstacle = codesters.Rectangle(
        random.randint(-1 * boundary, boundary), # x pos
        random.randint(-1 * boundary, boundary), # y pos
        random.randint(10, 30), # width
        random.randint(10, 30), # height
        "darkred",
    )

# ===== GOALS =====

"""
* Ships should fire
* Give shuttle movement
* Finish UFO movement
* Setup collision
* Obstacles should be sized properly
"""


# ===== NOTES =====

"""
sprite.event_key wants:
  1) a string (thing in quotes) to indicate what button to press
  2) a function (a set of instructions/recipe) to follow/run when the button is pressed

random.randint(a, b) gives a random 
number between a and b. no decimal point.
it can give a or b, or anything between

"""
