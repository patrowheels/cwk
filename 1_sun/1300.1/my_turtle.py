t = turtle.Turtle()

"""
# make a square
for i in range(4): # this will run 4 times
    t.forward(50)
    t.right(90)

# make a triangle
for i in range(3): # this will run 3 times
    t.forward(50)
    t.left(120)

"""

"""
# CIRCLE FUNCTION
def make_circle():
    for i in range(36):
        t.forward(5)
        t.left(10)
"""

# make a circle
for i in range(36):
    t.forward(5)
    t.left(10)

t.penup()
t.goto(-200, 200)
t.pendown()

# make a circle
for i in range(36):
    t.forward(5)
    t.left(10)

# TOGGLE PEN DOWN
# I don't expect you to understand how this works
"""
def toggle_pen():
    # toggle pen up/down
    if t.isdown():
        t.penup()
    else:
        t.pendown()
"""

