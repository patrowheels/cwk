sprite = codesters.Sprite("person1", -200, -50) # changed
stage.disable_all_walls()

touching_floor = True

def custom_gravity(sprite, y_acc): # added
    global touching_floor
    while sprite.get_y_speed() > -20 and not touching_floor:
        new_speed = sprite.get_y_speed() + y_acc # added
        sprite.set_y_speed(new_speed) # added
        stage.wait(.1)



def left_key():
    sprite.move_left(20)
    # add other actions...
    
stage.event_key("left", left_key)

def right_key():
    sprite.move_right(20)
    # add other actions...
    
stage.event_key("right", right_key)


def space_bar():
    global touching_floor
    sprite.jump(10)
    touching_floor = False
    custom_gravity(sprite, -1) # added
    # add other actions...
    
stage.event_key("space", space_bar)


# sprite = codesters.Rectangle(x, y, width, height, "color")

my_rectangle = codesters.Rectangle(0, -250, 600, 250, "orange") # changed



def collision_it(sprite, hit_sprite):
    my_var = hit_sprite.get_color() 
    if my_var == "orange":
        sprite.set_y_speed(0)
        touching_floor = True
    my_var = hit_sprite.get_image_name()
    if my_var == "anemone":
        sprite.say("Ouch!")
    
sprite.event_collision(collision_it)


"""
def collision(sprite, hit_sprite):
    my_var = hit_sprite.get_image_name() 
    if my_var == "codesters":
        sprite.say("I hit something!")
        
        
    # add any other actions...
    
sprite.event_collision(collision)
"""


for i in range(5):
    enemy = codesters.Sprite("anemone", 200 + i * 500, -100)
    enemy.set_x_speed(-5)
# make the girl fall down when she jumps


