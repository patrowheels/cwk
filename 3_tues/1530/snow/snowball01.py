hero = codesters.Sprite(
    "superhero2", # sprite
    -200, # x
    -190, # y
)
hero.set_size(.5)
villain = codesters.Sprite(
    "evilwizard", # sprite
    200, # x
    190, # y
)
villain.set_size(.6)

def left_key_minecraft(sprite):
    sprite.move_left(20)
    
hero.event_key("left", left_key_minecraft)

def right_key_roblox(sprite_fortnite):
    sprite_fortnite.move_right(20)
    
hero.event_key("right", right_key_roblox)
