# import random

choices = ["rock", "paper", "scissors"]

def click(_sprite):
    global user_choice
    user_choice = _sprite.get_image_name()
    print(user_choice)

choice_dict = {}
positions = [(-150, 150), (0, 150), (150, 150)]
for i in range(len(choices)):
    weapon_string = choices[i]
    x_pos = positions[i][0]
    y_pos = positions[i][1]
    choice_dict[choices[i]] = codesters.Sprite(
        weapon_string, x_pos, y_pos
    )

print(choice_dict)

for weapon in choice_dict.values():
    weapon.event_click(click)

user_choice = ""

while user_choice == "":
    stage.wait(.1)
    
comp_choice = random.choice(choices)

for one_choice in choices:
    if one_choice != user_choice and one_choice != comp_choice:
        stage.remove_sprite(choice_dict[one_choice])

chosen_weapon_text = codesters.Text(f"You chose {user_choice}", 0, -50)
stage.wait(2)
chosen_weapon_text.set_text(f"I chose {comp_choice}")
