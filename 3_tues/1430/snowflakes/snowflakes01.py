stage.set_background("winter")

def random_flake():
    options = [
        "snowflake1",
        "snowflake2",
        "snowflake3",
        "frosted_flake_c23"
    ]
    return random.choice(options)
    
new_flake = codesters.Sprite(
    random_flake(), # name
    0, # x
    0, # y
)

