# Commands
* `ls`
  * list all the files
* `cd`
  * change directory
* `clear`
  * clear the screen
  * `ctrl-l`
# Shell Concepts
* `tab-completion`
* `bash history/up arrow`