# WARMUP

* Make a file that asks the user for a letter, and asks the user for a word, and says if the letter is in the word

## Hints:
```python3
>>> user_answer = input("Question? ")
Question? I am Alex
>>> print(user_answer)
I am Alex
```
```python3
>>> print('a' in "abc")
True
>>> print('d' in "abc")
False
>>> print('A' in "abc")
False
```