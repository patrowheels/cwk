bricks = []
for y_pos in [200, 180, 160, 140]:
    for x_pos in [-200, -150, -100, -50, 0, 50, 100, 150, 200]:
        bricks.append(codesters.Rectangle(
            x_pos,# x pos
            y_pos,# y pos
            45,# width
            15,# height
            "darkred",
        ))

ball = codesters.Circle(
        3,    # x
        -100, # y
        25,   # diameter
        "black",
)

paddle = codesters.Rectangle(
    0, # x
    -200,# y
    120,# width
    20,# height
    "blue",# color
)

def left_key_frozen2(sprite_obi_wan):
    sprite_obi_wan.move_left(20)

paddle.event_key("left", left_key_frozen2)


def right_key_henry(sprite_danger):
    sprite_danger.move_right(20)

paddle.event_key("right", right_key_henry)


# Unnecessary Code
def say_position_minecraft(sprite_zelda):
    print(sprite_zelda.get_x())
    
paddle.event_key("a", say_position_minecraft)
