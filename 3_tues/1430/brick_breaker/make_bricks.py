# make_bricks.py

# similar to stuff codesters does for us
class Brick:
  def __init__(self, _x_pos, _y_pos):
    self.x_pos = _x_pos
    self.y_pos = _y_pos

  def print_pos(self):
    print(
      f"I am at x={self.x_pos}, y={self.y_pos}"
    )

bricks = []
for x in [-100, 0, 100]:
  for y in [200, 100, 0]:
    bricks.append(
      # similar to codesters.Sprite()
      # or codesters.Rectangle()
      Brick(_x_pos=x, _y_pos=y)
    )
for one_brick in bricks:
  one_brick.print_pos()