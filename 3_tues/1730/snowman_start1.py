# choose_word.py
import random
words = ['word1', 'word2', 'word3']
chosen_word = random.choice(words)
print(chosen_word)

# check_in_word.py
answer = "preview"
user_guess = input("What letter do you guess? ")
if user_guess in answer:
  print(f"{user_guess} is in the word!")
else:
  print(f"{user_guess} is not in the word!")

# make_lines.py
word = "preview"
# method 1
lines = "_ " * len(word)
print(lines)

# method 2
lines = ""
for one_letter in word:
  lines = lines + "_" + " "
print(lines)

# update_lines.py
answer = "preview"
lines = "_ " * len(answer)
user_guess = 'e'
lines_builder = ""
for one_letter in answer:
  if one_letter == user_guess:
    lines_builder += user_guess + " "
  else:
    lines_builder += "_" + " "
print(lines_builder)
