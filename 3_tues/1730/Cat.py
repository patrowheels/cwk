class Cat:
    def __init__(self, in_hunger):
        self.hunger = in_hunger

    def leap(self):
        print("The cat lept")
        self.hunger = self.hunger - 10
        print(f"The hunger level is {self.hunger}")

    def make_noise(self):
        if self.hunger <= 0:
            print("meow")
        else:
            print("purr")

    def feed(self):
        self.hunger = self.hunger + 30

