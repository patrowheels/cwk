words = [
    "roblox",
    "graveyard",
    "fire",
    "rickastley",
    "supercalifragilisticexpialidocious",
    "pneumonoultramicroscopicsilicovolcanoconiosis",
]

answer = random.choice(words)
print(answer)
representation = []
for one_letter in answer:
    representation.append("_")

representation_text = codesters.Text(
    " ".join(representation), 0, -100,
)

sprite = codesters.Sprite("alien1", 200, 0)

user_guess = sprite.ask("Guess a letter")

if user_guess in answer:
    sprite.say("Correct!")
    for i in range(len(answer)):
        if answer[i] == user_guess:
            representation[i] = user_guess
    representation_text.set_text(" ".join(representation))
