stage.disable_all_walls()

alien = codesters.Sprite("alien1")
alien.set_size(.2)

def left_key(sprite):
    sprite.set_x(sprite.get_x() - 20)
    sprite.say(sprite.get_x())

alien.event_key("left", left_key)

def right_key(sprite):
    sprite.set_x(sprite.get_x() + 20)
    sprite.say(sprite.get_x())

alien.event_key("right", right_key)

def up_key(sprite):
    sprite.set_y(sprite.get_y() + 20)
    
alien.event_key("up", up_key)

def down_key(sprite):
    sprite.set_y(sprite.get_y() - 20)
    
alien.event_key("down", down_key)

def make_train(x_pos):
    train = []
    
    for y_pos in [300, 350, 400]: # runs three times
        train.append(codesters.Rectangle(x_pos, y_pos, 20, 45, "blue"))

    for train_car in train:
        train_car.set_y_speed(-5)
        
    return train
    
while True:
    stage.wait(1)
    make_train(random.choice([-100, 0, 100]))
    
"""
# MORE CONFUSING WAY:

car_length = 45
gap_length = 5
combined_length = car_length + gap_length
for y_pos in range(0, combined_length * 2 + 1, combined_length):
    train.append(codesters.Rectangle(100, y_pos, 20, car_length, "blue"))
"""



