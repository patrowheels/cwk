# =============== SETUP STAGE ===============

stage.disable_all_walls()
stage.set_background("winter")
sman = codesters.Sprite("snowman", 0, -150)
sman.set_size(.3)

def left_key(sprite):
    sprite.move_left(20)
    # add other actions...
    
sman.event_key("left", left_key)

def right_key(sprite):
    sprite.move_right(20)
    # add other actions...
    
sman.event_key("right", right_key)

flake_sprites = []

while True:
    flake_list = ["snowflake1", "snowflake2", "snowflake3"]
    rand_flake = random.choice(flake_list)
    sprite = codesters.Sprite(
        rand_flake, # sprite name
        1000, # x
        1000, # y
    )
    sprite.set_size(.1 * random.randint(2, 5))
    sprite.hide()
    sprite.set_rotation(
        random.randint(0, 359)
    )
    sprite.go_to(
            random.randint(-225, 225), # x
            random.randint(200, 225), # y
    )
    sprite.set_y_speed(-1)
    sprite.show()

    flake_sprites.append(sprite)
    for one_flake in flake_sprites:
        """
        one_flake.set_x_speed(
            random.randint(-2, 2)
        )
        """
        if sman.get_x() > one_flake.get_x():
            one_flake.set_x_speed(
                random.randint(-1, 2)
            )
        else:
            one_flake.set_x_speed(
                random.randint(-2, 1)
            )
    stage.wait(0.1)
