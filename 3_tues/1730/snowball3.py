sprite = codesters.Sprite("robot", -200, -200)
def make_snowball():

    
    robot_x = sprite.get_x()
    robot_y = sprite.get_y()
    
    # compare the click position to the robot's position
    snowball_speed_x = stage.click_x() - robot_x
    snowball_speed_y = stage.click_y() - robot_y
    
    snowball_speed_x = snowball_speed_x / 10
    snowball_speed_y = snowball_speed_y / 10
    
    # make the snowball come from the robot
    snowball = codesters.Circle(robot_x, robot_y, 10, "white")
    

    snowball.set_outline_color("black")
    snowball.set_x_speed(snowball_speed_x)
    snowball.set_y_speed(snowball_speed_y)

stage.event_click(make_snowball)
stage.disable_all_walls()
