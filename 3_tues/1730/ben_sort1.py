"""
the_rock = codesters.Sprite("asteroid")

nums = [3, 8, 1, 5, 6]

smallest = nums[0]

for num in nums:
    if num < smallest:
        smallest = num
    
the_rock.say(smallest)
"""

the_rock = codesters.Sprite("asteroid")

nums = [3, 8, 1, 5, 6]
sorted_nums = [] # new
while len(nums) > 0: # new
    smallest = nums[0]
    
    for num in nums:
        if num < smallest:
            smallest = num
        
    the_rock.say(smallest) # delete
    sorted_nums.append(smallest) # new
    nums.remove(smallest) # new

the_rock.say(sorted_nums)
