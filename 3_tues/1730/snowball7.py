sprite = codesters.Sprite("robot", -200, -200)
sprite.health = 10
sprite.alive = True

enemy = codesters.Sprite("alien5", 175, 175)
enemy.set_size(.2)
enemy.health = 10
enemy.alive = True

def normalize(x_param, y_param):
    hypotenuse = math.sqrt(x_param**2 + y_param**2)
    x, y = x_param/hypotenuse, y_param/hypotenuse
    return x * 10, y * 10

def make_snowball():
    robot_x = sprite.get_x()
    robot_y = sprite.get_y()
    
    # compare the click position to the robot's position
    snowball_speed_x = stage.click_x() - robot_x
    snowball_speed_y = stage.click_y() - robot_y
    
    snowball_speed_x, snowball_speed_y = normalize(
        snowball_speed_x, snowball_speed_y
    )
    
    """
    snowball_speed_x = snowball_speed_x / 10
    snowball_speed_y = snowball_speed_y / 10
    """
    # make the snowball come from the robot
    snowball = codesters.Circle(robot_x, robot_y, 10, "white")
    snowball.owner = sprite
    snowball.set_outline_color("black")
    snowball.set_x_speed(snowball_speed_x)
    snowball.set_y_speed(snowball_speed_y)

stage.event_click(make_snowball)
stage.disable_all_walls()

def enemy_make_snowball():
    snowball = codesters.Circle(enemy.get_x(), enemy.get_y(), 10, "white")
    snowball.owner = enemy
    snowball.set_outline_color("black")
    snowball.set_x_speed(
        random.randint(-4, -1)    
    )
    snowball.set_y_speed(
        random.randint(-4, -1)    
    )
    

def up_key(sprite):
    if sprite.get_y() < -50:
        sprite.move_up(20)

def down_key(sprite):
    sprite.move_down(20)

def left_key(sprite):
    sprite.move_left(20)

def right_key(sprite):
    if sprite.get_x() < -50:
        sprite.move_right(20)

sprite.event_key("up", up_key)
sprite.event_key("down", down_key)
sprite.event_key("left", left_key)
sprite.event_key("right", right_key)


def collision(in_sprite, hit_sprite):
    my_var = hit_sprite.get_color() 
    if my_var == "white" and hit_sprite.owner != in_sprite:
        in_sprite.health -= 1
        stage.remove_sprite(hit_sprite)
    
sprite.event_collision(collision)
enemy.event_collision(collision)



game_running = True
while game_running == True:
    sprite.say(sprite.health)
    enemy.say(enemy.health)
    stage.wait(.1 * random.randint(4, 10))
    enemy_make_snowball()
    if sprite.health <= 0 or enemy.health <= 0:
        game_running = False

text = codesters.Text("Game Over!")




