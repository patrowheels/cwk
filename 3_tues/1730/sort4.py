def swap(in_list, index1, index2):
    
    """Swaps items at index1 and index2"""
    
    in_list[index1], in_list[index2] = (
        in_list[index2],
        in_list[index1]
    )

def generate_random_number_list():
    
    """creates a shuffled list of numbers, 101 thru 110"""
    
    nums = list(range(101, 111))
    random.shuffle(nums)
    return nums
    

def generate_mostly_sorted_list():
    
    """creates a mostly sorted list of numbers
    with only two numbers out of order
    because of one swap
    the number at index 0 is always swapped
    with another random index""" 
    
    nums = list(range(101, 111))
    randindex = random.randrange(1, len(nums))
    swap(nums, 0, randindex)
    return nums
 
def simple_sort_new_list(unsorted_list):
    sorted_list = []
    while len(unsorted_list) > 0:
        smallest_num = unsorted_list[0]
        smallest_num_index = 0
        for index, num in enumerate(unsorted_list):
            if num < smallest_num:
                smallest_num = num
                smallest_num_index = index
        sorted_list.append(smallest_num) # add the smallest num to the sorted list
        unsorted_list.pop(smallest_num_index) # remove the smallest num from the unsorted list
    return sorted_list


def simple_sort_new_list(blue_box):
    yellow_box = []
    while len(blue_box) > 0: # while there's stuff in the blue box
        smallest_num = blue_box[0] # first num in blue box
        smallest_num_index = 0 # first index in blue box
        for index, num in enumerate(blue_box): # look at everything in the blue box
            if num < smallest_num:
                smallest_num = num # remember the smallest num in the blue box
                smallest_num_index = index # remember the index of the smallest num
        yellow_box.append(smallest_num) # add the smallest num to the yellow box
        blue_box.pop(smallest_num_index) # remove the smallest num from the blue box
    return yellow_box

sprite = codesters.Sprite("alien1")
random_number_list = generate_random_number_list()
sorted_list = simple_sort_new_list(random_number_list)
sprite.say(sorted_list)
