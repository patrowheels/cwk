stage.disable_all_walls()

def setup():
    global lives
    global lives_text
    global alien
    global brain
    # ========== SETUP ALIEN ==========
    alien = codesters.Sprite("alien1", -220, 0) # changed
    alien.set_size(.2)
    alien.invincible = False
    
    # ========== SETUP BRAIN ==========
    brain = codesters.Circle(100, 100, 10, "pink")
    
    # ========== SETUP LIVES ==========
    lives = 3
    lives_text = codesters.Text(f"lives: {lives}", -200, 200)
    alien.event_collision(collision)
    
    alien.event_key("left", left_key)
    alien.event_key("right", right_key)
    alien.event_key("up", up_key)
    alien.event_key("down", down_key)
    
    
# ========== SETUP MOVEMENT ==========
def left_key(sprite):
    sprite.set_x(sprite.get_x() - 20)
    sprite.say(sprite.get_x())


def right_key(sprite):
    sprite.set_x(sprite.get_x() + 20)
    sprite.say(sprite.get_x())


def up_key(sprite):
    sprite.set_y(sprite.get_y() + 20)
    

def down_key(sprite):
    sprite.set_y(sprite.get_y() - 20)
    

# ========== SETUP COLLISION ==========
def collision(me, not_me):
    global lives
    not_me_color = not_me.get_color()
    
    if not_me_color == "blue" and me.get_x() == not_me.get_x(): # collide with train
        if not alien.invincible:
            alien.invincible = True
            me.say("Ouch!")
            # me.set_height(5)
            lives = lives - 1
            lives_text.set_text(f"lives: {lives}")
            stage.wait(1)
            alien.invincible = False

    elif not_me_color == "pink": # collide with brain
        lives += 1
        me.set_height(me.get_height() + 5)
        lives_text.set_text(f"lives: {lives}")
        not_me.set_x(random.randint(-10, 10) * 20)
        not_me.set_y(random.randint(-10, 10) * 20)



# ========== SETUP TRAIN ==========
def make_train(x_pos):
    train = []
    
    for y_pos in [300, 350, 400]: # runs three times
        # each train will have a different y position
        train.append(codesters.Rectangle(x_pos, y_pos, 20, 45, "blue"))

    for train_car in train:
        # make every blue rectangle fall
        train_car.set_y_speed(-5)
    return train


def restart():
    stage.remove_sprite(restart_button)
    restart_text.set_text("")
    stage.remove_sprite(restart_text)
    
    stage.remove_sprite(alien)
    setup()
    run_game()

def run_game():
    global restart_button
    global restart_text
    counter = 0
    game_running = True # changed
    while game_running == True: # changed
        counter = counter + 1
        # every one second, create a train
        # with a random left-right position
        stage.wait(0.1)
        if counter % 10 == 0:
            make_train(random.choice([-200, -100, 0, 100, 200])) # changed
        if lives <= 0:
            game_running = False
    stage.wait(2)
    restart_button = codesters.Rectangle(0, 0, 200, 100, "green")
    restart_text = codesters.Text("restart", 0, 0)
    restart_button.event_click(restart)
    
setup()
run_game()

# MORE CONFUSING WAY:
"""
car_length = 45
gap_length = 5
combined_length = car_length + gap_length
for y_pos in range(0, combined_length * 2 + 1, combined_length):
    train.append(codesters.Rectangle(100, y_pos, 20, car_length, "blue"))
"""

"""
GOALS:
    * Make a button to restart the game
    * Make everything stop once the sprite is squashed
    * Multiple levels
    * Trains that go up from the bottom
    * Delete trains that go to far
"""
