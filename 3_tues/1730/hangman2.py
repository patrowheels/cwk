import random
def random_word_fun():
    words = "apple aardvark arizona alligator".split()
    random_word = random.choice(words)
    return random_word

def generate_replace(positions_param, active_word):
    output = ""
    for i in range(len(active_word)):
        if positions_param[i] == True:
            output += active_word[i] + " "
        else:
            output += "_ "
    return output

game_running = True

frank = codesters.Sprite("frankenstein", 180, 150)

guessed_letters = []
active_word = random_word_fun()
guessed_positions = []
for letter in active_word:
    guessed_positions.append(False)
print(guessed_positions)
guessed_output = codesters.Text("", -150, 0)
presented_word = codesters.Text("", -150, 190)

while game_running == True:
    print(active_word)
    word_length = len(active_word)
    presented_word.set_text(generate_replace(guessed_positions, active_word))
    output = "Guessed letters:\n" + ", ".join(guessed_letters)
    guessed_output.set_text(output)
    user_guess = frank.ask("Guess a letter: ")
    print(user_guess, active_word)
    if user_guess not in guessed_letters and user_guess in active_word: 
        for i in range(len(active_word)):
            if user_guess == active_word[i]:
                guessed_positions[i] = True
    guessed_letters.append(user_guess)
    




