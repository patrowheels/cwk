# update_lines_two_strings.py

import random

# ===== GUESS SETUP =====

answer = "sonic"
wrong_letters = "xyz"
possible_guesses = list(answer + wrong_letters)
random.shuffle(possible_guesses)
print(possible_guesses)
old_lines = '_' * len(answer)

# ===== GAME LOOP =====
running = True
while running:
  current_guess = possible_guesses.pop()
  print(f"Guessing {current_guess}")
  new_lines = ''
  for index in range(len(old_lines)):
    if old_lines[index] != "_":
      new_lines = new_lines + old_lines[index]
    else:
      if answer[index] == current_guess:
        new_lines = new_lines + current_guess
      else:
        new_lines = new_lines + "_"
  if len(possible_guesses) == 0:
    running = False
  old_lines = new_lines
  print(new_lines)
