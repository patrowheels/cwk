alien = codesters.Sprite("alien1")
alien.set_size(.2)

def left_key(sprite):
    sprite.set_x(sprite.get_x() - 20)
    sprite.say(sprite.get_x())

alien.event_key("left", left_key)

def right_key(sprite):
    sprite.set_x(sprite.get_x() + 20)
    sprite.say(sprite.get_x())

alien.event_key("right", right_key)

def up_key(sprite):
    sprite.set_y(sprite.get_y() + 20)
    
alien.event_key("up", up_key)

def down_key(sprite):
    sprite.set_y(sprite.get_y() - 20)
    
alien.event_key("down", down_key)

train = []



for y_pos in [0, 50, 100]: # runs three times
    train.append(codesters.Rectangle(100, y_pos, 20, 45, "blue"))
"""
# MORE CONFUSING WAY:

car_length = 45
gap_length = 5
combined_length = car_length + gap_length
for y_pos in range(0, combined_length * 3, combined_length):
    train.append(codesters.Rectangle(100, y_pos, 20, car_length, "blue"))
"""
