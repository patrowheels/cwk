obstacles = [] # changed
stage.set_gravity(5) # lowered gravity
game_over = False
player = codesters.Circle(-100, 25, 50, "blue")
on_the_ground = True

def initialize():
    stage.set_background_color("lightblue")
    ground = codesters.Rectangle(0, -200, 800, 400, "limegreen")
    ground.set_gravity_off()

def create_obstacle():
    global obstacles # changed
    for i in range(5): # changed
        obstacle = codesters.Rectangle(i*200 + 200, 0, 50, 100, "red") # changed
        obstacle.set_gravity_off()
        obstacles.append(obstacle) # changed

create_obstacle()

def collision(player, hit_sprite):
    global on_the_ground
    on_the_ground = True
    player.set_y_speed(0)
    player.set_gravity_off()
    
    if hit_sprite.get_color() == "red":
        endGame()
player.event_collision(collision)

initialize()

def space_bar():
    global on_the_ground
    if on_the_ground == True:
        on_the_ground = False
        player.jump(10) # lowered jump
        player.set_gravity_on()

stage.event_key("space", space_bar)

def endGame():
    global game_over
    game_over = True

counter = 0
while game_over == False:
    # moves right
    counter += 1
    print(counter)
    for obstacle in obstacles: # changed
        obstacle.set_x(obstacle.get_x() - 5) # changed
    stage.wait(0.05)
