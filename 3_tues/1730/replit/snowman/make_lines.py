word = "preview"
"""
# method 1
lines = "_ " * len(word)
# lines = "_ " * 7
# lines = "_ _ _ _ _ _ _ "
print(lines)
"""




# method 2
lines = ""
for one_letter in word:
  lines = lines + "_" + " "
print(lines)