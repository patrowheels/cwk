answer = "mississippi"
lines = "_ " * len(answer)
user_guess = 's'
lines_builder = ""
for one_letter in answer:
  if one_letter == user_guess:
    lines_builder += user_guess + " "
  else:
    lines_builder += "_" + " "
print(lines_builder)