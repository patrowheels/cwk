# Snowman game
* make a list of possible words
* randomly choose one of the words
* display some lines to indicate what the user can guess
* let the user guess a letter
  * if the user guesses correctly, update the lines so that the correct lines are replaced by the letter
  * if the user guesses incorrectly, add the incorrect letter to a list
* draw a snowman