villain = codesters.Sprite(
    "evilwizard", # sprite
    199, # x
    199, # y
)
villain.set_size(.5)

villain_bg = codesters.Circle(
    villain.get_x(), # x
    villain.get_y(), # y
    175, # radius
    "darkred",
)
villain_bg.move_to_back()

hero = codesters.Sprite(
    "dolphin1", # sprite
    -199, # x
    -199, # y
)

hero.flip_right_left()
hero.set_size(.3)
hero_bg = codesters.Circle(
    hero.get_x(), # x
    hero.get_y(), # y
    175, # radius
    "lightblue",
)
hero_bg.move_to_back()



