stage.disable_all_walls()

villain = codesters.Sprite(
    "evilwizard", # sprite
    199, # x
    199, # y
)
villain.set_size(.5)

villain_bg = codesters.Circle(
    villain.get_x(), # x
    villain.get_y(), # y
    175, # radius
    "darkred",
)
villain_bg.move_to_back()

hero = codesters.Sprite(
    "dolphin_thing_78b", # sprite
    -199, # x
    -199, # y
)

# hero.flip_right_left()
hero.set_size(.5)
hero_bg = codesters.Circle(
    hero.get_x(), # x
    hero.get_y(), # y
    175, # radius
    "lightblue",
)
hero_bg.move_to_back()

def normalize(x_speed, y_speed):
    hypotenuse = math.sqrt(
        x_speed ** 2 +
        y_speed ** 2
    )
    return (
        x_speed / hypotenuse,
        y_speed / hypotenuse
    )

def make_snowball():
    snowball = codesters.Circle(
        hero.get_x(), # x
        hero.get_y(), # y
        50, # radius
        "gray", # color
    )
    x_speed = (stage.click_x() - hero.get_x()) # CHANGED
    y_speed = (stage.click_y() - hero.get_y()) # CHANGED
    x_speed, y_speed = normalize(x_speed, y_speed)# NEW LINE
    snowball.set_x_speed(x_speed * 10) # CHANGED
    snowball.set_y_speed(y_speed * 10) # CHANGED
    
    
stage.event_click(make_snowball)



