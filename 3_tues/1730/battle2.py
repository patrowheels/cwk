# ===== SETUP STAGE =====

stage.set_background("space4")
stage.disable_all_walls()

# ===== SETUP SPRITES =====

ufo = codesters.Sprite("ufo", 200, -200)
ufo.set_size(.3)
shuttle = codesters.Sprite("spaceshuttle", -200, 200)
shuttle.set_size(.3)

# ===== SETUP MOVEMENT =====

def up_key(sprite):
    sprite.move_up(20)
    sprite.set_rotation(90)

ufo.event_key("up", up_key)

def down_key(sprite):
    sprite.move_down(20)
    sprite.set_rotation(270)

ufo.event_key("down", down_key)

def left_key(sprite):
    sprite.move_left(20)
    sprite.set_rotation(180)

ufo.event_key("left", left_key)


def right_key(sprite):
    sprite.move_right(20)
    sprite.set_rotation(0)

ufo.event_key("right", right_key)

# ===== SETUP FIRING =====

def fire_green():
    green = codesters.Sprite("green_laser_34a", 400, 400)
    green.set_size(.2)

    if ufo.get_rotation() in (90, 270):
        green.set_rotation(90)
    
    green.go_to(
        ufo.get_x(),
        ufo.get_y(),
    )
    

    if ufo.get_rotation() == 0:
        green.set_y_speed(-10)
    elif ufo.get_rotation() == 180:
        green.set_y_speed(10)
    elif ufo.get_rotation() == 90:
        # green.set_rotation(90)
        green.set_x_speed(10)
    elif ufo.get_rotation() == 270:
        # green.set_rotation(90)
        green.set_x_speed(-10)
    

while True:
    stage.wait(1)
    fire_green()

