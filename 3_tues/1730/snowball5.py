sprite = codesters.Sprite("robot", -200, -200)

def normalize(x_param, y_param):
    hypotenuse = math.sqrt(x_param**2 + y_param**2)
    x, y = x_param/hypotenuse, y_param/hypotenuse
    return x * 10, y * 10

def make_snowball():

    
    robot_x = sprite.get_x()
    robot_y = sprite.get_y()
    
    # compare the click position to the robot's position
    snowball_speed_x = stage.click_x() - robot_x
    snowball_speed_y = stage.click_y() - robot_y
    
    snowball_speed_x, snowball_speed_y = normalize(
        snowball_speed_x, snowball_speed_y
    )
    
    """
    snowball_speed_x = snowball_speed_x / 10
    snowball_speed_y = snowball_speed_y / 10
    """
    # make the snowball come from the robot
    snowball = codesters.Circle(robot_x, robot_y, 10, "white")
    

    snowball.set_outline_color("black")
    snowball.set_x_speed(snowball_speed_x)
    snowball.set_y_speed(snowball_speed_y)

stage.event_click(make_snowball)
stage.disable_all_walls()

def up_key(sprite):
    if sprite.get_y() < -50:
        sprite.move_up(20)

def down_key(sprite):
    sprite.move_down(20)

def left_key(sprite):
    sprite.move_left(20)

def right_key(sprite):
    if sprite.get_x() < -50:
        sprite.move_right(20)

sprite.event_key("up", up_key)
sprite.event_key("down", down_key)
sprite.event_key("left", left_key)
sprite.event_key("right", right_key)
