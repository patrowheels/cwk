# ===== SETUP STAGE =====

stage.set_background("space4")
stage.disable_all_walls()

# ===== SETUP SPRITES =====

ufo = codesters.Sprite("ufo", 200, -200)
ufo.offset = 90
ufo.set_size(.3)

shuttle = codesters.Sprite("spaceshuttle", -200, 200)
shuttle.set_size(.3)
shuttle.offset = 45
shuttle.set_rotation(0 + shuttle.offset)

def ufo_collision(sprite, hit_sprite):
    sprite_name = hit_sprite.get_image_name()
    if sprite_name == "red_laser_19d":
        sprite.say("I hit something!")
        
ufo.event_collision(ufo_collision)

def shuttle_collision(sprite, hit_sprite):
    sprite_name = hit_sprite.get_image_name()
    if sprite_name == "green_laser_34a":
        sprite.say("I hit something!")
        
shuttle.event_collision(shuttle_collision)

# ===== SETUP MOVEMENT =====

def up_key(sprite):
    sprite.move_up(20)
    sprite.set_rotation(90)

ufo.event_key("up", up_key)

def down_key(sprite):
    sprite.move_down(20)
    sprite.set_rotation(270)

ufo.event_key("down", down_key)

def left_key(sprite):
    sprite.move_left(20)
    sprite.set_rotation(180)

ufo.event_key("left", left_key)


def right_key(sprite):
    sprite.move_right(20)
    sprite.set_rotation(0)

ufo.event_key("right", right_key)

# ===== SETUP FIRING =====

def collision(sprite, hit_sprite):
    my_var = hit_sprite.get_color() 
    if my_var == "darkred":
        stage.remove_sprite(sprite)
        
def fire_green():
    green = codesters.Sprite("green_laser_34a", 400, 400)
    green.set_size(.2)
    green.event_collision(collision)

    """
    if ufo.get_rotation() in (90, 270):
        green.set_rotation(90)
    """
    
    green.go_to(
        ufo.get_x(),
        ufo.get_y(),
    )
    
    if ufo.get_rotation() == 0:
        green.set_y_speed(-10)
    elif ufo.get_rotation() == 180:
        green.set_y_speed(10)
    elif ufo.get_rotation() == 90:
        green.set_rotation(90)
        green.set_x_speed(10)
    elif ufo.get_rotation() == 270:
        green.set_rotation(90)
        green.set_x_speed(-10)
    
    """
    if ufo.get_rotation() == 360 - ufo.offset:
        green.set_y_speed(-10)
    elif ufo.get_rotation() == 180 - ufo.offset:
        green.set_y_speed(10)
    elif ufo.get_rotation() == 90 - ufo.offset:
        green.set_rotation(90)
        green.set_x_speed(10)
    elif ufo.get_rotation() == 270 - ufo.offset:
        green.set_rotation(90)
        green.set_x_speed(-10)
    """

# ===== SETUP OBSTACLES =====

for i in range(50):
    obstacle = codesters.Rectangle(
        random.randint(-175, 175), # x position
        random.randint(-175, 175), # y position
        random.randint(10, 25), # width
        random.randint(10, 25), # height
    "darkred")


counter = 0
while True:
    counter = counter + 1
    stage.wait(0.1)
    if counter % 5 == 0:
        fire_green()


"""
GOALS:
* Give players health
* Make it so you can move and fire with the shuttle
* Add collisions
* You can shoot the beam in different locations
"""
