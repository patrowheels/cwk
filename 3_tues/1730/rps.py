sprite = codesters.Sprite("alien1")
user_choice = sprite.ask("Choose: rock, paper, or scissors")
computer_choice = "rock"
sprite.say(f"Computer chose {computer_choice}", 2)

# computer chooses rock
if user_choice == "rock":
    sprite.say("Draw!")
elif user_choice == "paper":
    sprite.say("I lose!")
elif user_choice == "scissors":
    sprite.say("I win!")
