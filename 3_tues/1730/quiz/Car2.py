class Car:
    def __init__(self, _color, _size):
        print("I am being created")
        self.color = _color
        self.size = _size
        
    # less important
    def announce(self):
        print("I am a", self.size, self.color, "car")
        
carry = Car(_color = "silver", _size = "medium")
midnight = Car(_color = "black", _size = "medium")
navy = Car(_color = "dark blue", _size = "medium")


# print("one car is", carry.color)
# print("one car is", midnight.color)
# print("one car is", navy.color)

carry.announce()
midnight.announce()
navy.announce()
