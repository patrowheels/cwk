stage.set_background("concert")

# blueprint
class Question:
    def __init__(
        self, # NEW LINE
        _prompt,
        _a,
        _b,
        _c,
        _d,
        _answer,
        ):
        self.prompt = _prompt
        self.a = _a
        self.b = _b
        self.c = _c
        self.d = _d
        self.answer = _answer
        
question_list = [
    Question(
        _prompt = "How many MLB baseball teams are there?",
        _a = "18",
        _b = "20",
        _c = "25",
        _d = "30",
        _answer = "30",
    ),
    Question(
        _prompt = "How old is Saanvi?",
        _a = "12",
        _b = "13",
        _c = "14",
        _d = "15",
        _answer = "13",
    ),
    Question(
        _prompt = "How many games are on Nintendo 64?",
        _a = "393",
        _b = "412",
        _c = "465",
        _d = "519",
        _answer = "393",
    ),
]


prompt_text = codesters.Text(
    "question", # text
    0, # x
    150, # y
)

guess_a = codesters.Text(
    "a", # text
    -150, # x
    100, # y
)

guess_b = codesters.Text(
    "b", # text
    50, # x
    100, # y
)

guess_c = codesters.Text(
    "c", # text
    -150, # x
    0, # y
)

guess_d = codesters.Text(
    "d", # text
    50, # x
    0, # y
)

for one_text_thing in [
    prompt_text,
    guess_a,
    guess_b,
    guess_c,
    guess_d,]:
    one_text_thing.set_color("white")


waiting = True


def set_waiting_false():
    global waiting
    waiting = False

stage.event_click(set_waiting_false)

for one_question in question_list:
    prompt_text.set_text(
        one_question.prompt
    )
    guess_a.set_text(one_question.a)
    guess_b.set_text(one_question.b)
    guess_c.set_text(one_question.c)
    guess_d.set_text(one_question.d)


    waiting = True
    while waiting == True:
        stage.wait(0.1)

prompt_text.set_text("Done!")

