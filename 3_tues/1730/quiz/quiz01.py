stage.set_background("beach3")

# blueprint
class Question:
    def __init__(
        _prompt,
        _a,
        _b,
        _c,
        _d,
        _answer,
        ):
        self.prompt = _prompt
        self.a = _a
        self.b = _b
        self.c = _c
        self.d = _d
        self.answer = _answer
