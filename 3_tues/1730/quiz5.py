stage.set_background_color('black')
sprite = codesters.Sprite("alien1", 0, -200)

score = 0
question = codesters.Text("test", 0, 180, "lightblue")
a = codesters.Text("A:", -150, 50, "lightblue")
b = codesters.Text("B:", 50, 50, "lightblue")
c = codesters.Text("C:", -150, -50, "lightblue")
d = codesters.Text("D:", 50, -50, "lightblue")
display_questions = [a, b, c, d]

question_list = []
answer_list = []
correct_answer_list = []

def setup_questions():
    # all associated with session=0
    question_list.append("How many seasons are there?")
    answer_list.append(
        ["2", "3", "4", "5"]
    )
    correct_answer_list.append("c")
    
    # all associated with session=1
    question_list.append("How many seasons of Naruto are there?")
    answer_list.append(
        ["2", "4", "6", "8"]
    )
    correct_answer_list.append("b")
    
    # all associated with session=2
    question_list.append("What is my teacher's name?")
    answer_list.append(
        ["Axel", "Abby", "Ack", "Alex"]
    )
    correct_answer_list.append("d")
    
    # all associated with session=3
    question_list.append("What's the best place to shop?")
    answer_list.append(
        ["Target", "Amazon", "Wal*Mart", "7/11"]
    )
    correct_answer_list.append("a")
    
    question_list.append("Who's the best speedcuber in the world?")
    answer_list.append(
        ["Sally", "Ruihang", "Fred", "Max"]
    )
    correct_answer_list.append("b")

def ask_questions():
    global score
    for session in range(len(question_list)):
    # for session in [0, 1, 2, 3]:
        question.set_text(question_list[session])
        """
        node_list = [a, b, c, d]
        for i in range(len(node_list)):
            node_list[i].set_text(
                "{}: {}".format("ABCD"[i], answer_list[session][i])
            )
        """
        a.set_text(
            "A: {}".format(answer_list[session][0])
        )
        b.set_text(
            "B: {}".format(answer_list[session][1])
        )
        c.set_text(
            "C: {}".format(answer_list[session][2])
        )
        d.set_text(
            "D: {}".format(answer_list[session][3])
        )
        user_answer = stage.ask("what's the answer?", 0, -200).lower()
        print(correct_answer_list[session])
        if correct_answer_list[session] == user_answer:
            score += 1
            sprite.say("Correct")
        else:
            sprite.say("Incorrect")
        # if correct_answer_list[session] != user_answer:
        #    sprite.say("Incorrect")
        stage.wait(2)
        sprite.say("")

def dance(in_sprite):
    for i in range(4):
        """
        in_sprite.set_rotation(20)
        stage.wait(1)
        in_sprite.set_rotation(340)
        stage.wait(1)
        """
        for angle in [20, 340]:
            in_sprite.set_rotation(angle)
            stage.wait(1)
        
def die(in_sprite):
    """
    in_sprite.set_rotation(330)
    stage.wait(1)
    in_sprite.set_rotation(310)
    stage.wait(1)
    in_sprite.set_rotation(290)
    stage.wait(1)
    in_sprite.set_rotation(270)
    """
    for angle in [330, 310, 290, 270]:
        in_sprite.set_rotation(angle)
        stage.wait(1)

def clear_questions(in_list):
    for one_text in in_list:
        one_text.set_text("")
    """
    what happens in the two lines above:
    question.set_text("")
    a.set_text("")
    b.set_text("")
    c.set_text("")
    d.set_text("")
    """

def announce_score():
    global score
    sprite.say(f"Your score is {score}")
    if score < 3:
        die(sprite)
    else:
        dance(sprite)


setup_questions()
ask_questions()
clear_questions([question, a, b, c, d])
announce_score()
